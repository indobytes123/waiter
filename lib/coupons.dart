import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

class Coupons extends StatefulWidget {
  @override
  _CouponsState createState() => _CouponsState();
}

class _CouponsState extends State<Coupons> {
  final List<String> tiffins = [
    "idly",
    "vada",
    "poori",
    "dosa",
    "manchuraian",
    "panipoori",
    "idly",
    "idly",
    "idly",
  ];

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 60,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: false,
          title: Text("Apply  Coupons",
              style: new TextStyle(
                  color: login_passcode_text,
                  fontSize: 24.0,
                  fontWeight: FontWeight.w500)),
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 10.0),
                icon:
                    Image.asset("images/back_arrow.png", width: 22, height: 22),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            },
          ),
        ),
        body: Center(
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                color: Colors.white,
                child: Container(
                  color: dashboard_bg,
                  margin: EdgeInsets.all(5),
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.rectangle,
                            boxShadow: [
                              BoxShadow(
                                color: login_passcode_box,
                                blurRadius: 2.0,
                                spreadRadius: 1.0,
                              ),
                            ]),
                        margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                        child: ListTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                flex: 4,
                                child: Container(
                                    margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                    height: 42,
                                    child: TextField(
                                      onChanged: (string) {
                                        setState(() {
                                          if (string.length <= 0) {}
                                        });
                                      },
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          bottom: 30 / 2,
                                          left:
                                              9 / 2, // HERE THE IMPORTANT PART
                                          // HERE THE IMPORTANT PART
                                        ),
                                        hintText: 'Coupon Code',
                                        hintStyle: TextStyle(
                                            fontSize:
                                                SizeConfig.safeBlockHorizontal *
                                                    3.6,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black12),
                                        filled: true,
                                        fillColor: Color(0xffffffff),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(4.0)),
                                          borderSide:
                                              BorderSide(color: Colors.white),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(4.0)),
                                          borderSide:
                                              BorderSide(color: Colors.white),
                                        ),
                                      ),
                                    )),
                              ),
                              Expanded(
                                flex: 1,
                                child: InkWell(
                                  onTap: () {},
                                  child: Text(
                                    "Apply",
                                    style: TextStyle(
                                      color: Colors.lightBlueAccent,
                                      fontSize:
                                          SizeConfig.safeBlockHorizontal * 3.4,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Card(
                            margin: EdgeInsets.fromLTRB(15, 5, 15, 15),
                            elevation: 5,
                            child: Container(
                              color: Colors.white,
                              child: Column(children: [
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: Padding(
                                      padding:
                                          EdgeInsets.fromLTRB(15, 15, 15, 0),
                                      child: Text(
                                        "Available Coupons",
                                        style: TextStyle(
                                            color: login_passcode_bg1,
                                            fontSize: 16,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w600),
                                        textAlign: TextAlign.left,
                                      )),
                                ),
                                Container(
                                  margin:
                                      EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                  child: Divider(
                                    color: cart_viewline,
                                  ),
                                ),
                                Expanded(
                                  child: ListView.separated(
                                      separatorBuilder: (context, index) =>
                                          Divider(
                                            color: Colors.grey,
                                            height: 1.0,
                                          ),
                                      scrollDirection: Axis.vertical,
                                      itemCount: tiffins.length,
                                      itemBuilder: (context, index) {
                                        return Container(
                                          margin: EdgeInsets.all(2),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.3,
                                          height: 150,
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Container(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            10, 0, 15, 0),
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Image.asset(
                                                          "images/coupons.png",
                                                          height: 48,
                                                          width: 48,
                                                        ),
                                                        Text(
                                                          "FCH50",
                                                          style: TextStyle(
                                                              color:
                                                                  login_passcode_text,
                                                              fontSize: 16,
                                                              fontFamily:
                                                                  'Poppins',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                          textAlign:
                                                              TextAlign.left,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0, 0, 15, 0),
                                                    child: InkWell(
                                                      onTap: () {},
                                                      child: Text(
                                                        "Apply",
                                                        style: TextStyle(
                                                          color: Colors
                                                              .lightBlueAccent,
                                                          fontSize: SizeConfig
                                                                  .safeBlockHorizontal *
                                                              3.4,
                                                          fontFamily: 'Poppins',
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Container(
                                                  alignment: Alignment.topLeft,
                                                  child: Padding(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              15, 0, 15, 0),
                                                      child: Text(
                                                        "Get 15% cashback using Freecharge",
                                                        style: TextStyle(
                                                            color: coupontext,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                'Poppins',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                        textAlign:
                                                            TextAlign.left,
                                                      ))),
                                              Container(
                                                padding: EdgeInsets.fromLTRB(
                                                    10, 10, 10, 0),
                                                child: Row(
                                                  children: List.generate(
                                                      150 ~/ 2,
                                                      (index) => Expanded(
                                                            child: Container(
                                                              color: index %
                                                                          2 ==
                                                                      0
                                                                  ? Colors
                                                                      .transparent
                                                                  : Colors.grey,
                                                              height: 1,
                                                            ),
                                                          )),
                                                ),
                                              ),
                                              Container(
                                                  alignment: Alignment.topLeft,
                                                  child: Padding(
                                                      padding:
                                                          EdgeInsets.fromLTRB(
                                                              15, 15, 15, 0),
                                                      child: Text(
                                                        "Use code FCH50 and get 15% off cashback up to " +
                                                            new String
                                                                    .fromCharCodes(
                                                                new Runes(
                                                                    '\u0024')) +
                                                            "5 on the first ever transaction using Freecharge.",
                                                        style: TextStyle(
                                                            color:
                                                                coupontextdesc,
                                                            fontSize: 12,
                                                            fontFamily:
                                                                'Poppins',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                        textAlign:
                                                            TextAlign.left,
                                                      )))
                                            ],
                                          ),
                                        );
                                      }),
                                ),
                              ]),
                            )),
                      )
                    ],
                  ),
                ))));
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
