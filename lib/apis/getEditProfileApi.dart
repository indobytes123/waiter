import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getallguesttableresponse.dart';
import 'package:waiter/model/getprofileapiresponse.dart';
import 'package:waiter/model/gettableinformationresponse.dart';
import 'package:waiter/utils/all_constans.dart';
class GetEditProfileApiRepository {

  Future<getprofileapiresponse> editprofile(String employeeId, String restaurantId,String firstName, String lastName, String currentPassword,String newPassword, String confirmPassword) async {
    var body = json.encode({'employeeId': employeeId, 'restaurantId': restaurantId, 'firstName': firstName, 'lastName': lastName, 'currentPassword': currentPassword,'newPassword': newPassword, 'confirmPassword': confirmPassword});

    print(base_url + "get_profile"+ "edit_profileAPIREQUEST"+"------"+employeeId+"----"+restaurantId+"====="+firstName+"====="+lastName+"====="+currentPassword+"====="+newPassword+"====="+confirmPassword);
    dynamic response = await Requests.post(base_url + "edit_profile",
        body: body, headers: {'Content-type': 'application/json'});
    print(base_url + ""+ response.toString());
    getprofileapiresponse tableinformation_data =
    getprofileapiresponse.fromJson(jsonDecode(response));
    return tableinformation_data;
  }
}
