import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getallvoidreasonsresponse.dart';
import 'package:waiter/utils/all_constans.dart';

class GetAllVoidReasonsApiRepository {

  Future<List<VoidReasonsList>> getallvoidreasons(String employeeId,
      String restaurantId) async {
    var body = json.encode(
        {'restaurantId': restaurantId, 'employeeId': employeeId});
    print(base_url + "get_all_void_reasons" + "VOIDREASONAPIREQUEST" +
        employeeId + "----" + restaurantId);

    dynamic response = await Requests.post(base_url + "get_all_void_reasons",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "VOIDREASONSSRESPONSE" + response.toString());
    final res = json.decode(response);
    Iterable list = res['void_reasonsList'];
    return list.map((model) => VoidReasonsList.fromJson(model)).toList();
  }


}


