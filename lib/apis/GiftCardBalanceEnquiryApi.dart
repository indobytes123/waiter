import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/giftcardresponse.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class GiftCardBalanceEnquiryRepository {
  Future<giftcardresponse> giftCardBalanceEnquiry(int giftCardNo, String restaurantId) async {
    var body = json.encode({
      'giftCardNo': giftCardNo,
      'restaurantId': restaurantId
    });
    print(base_url + " GIFTCARD_REQUEST " + body.toString());

    dynamic response = await Requests.post(base_url + "giftcard_balance_enquiry",
        body: body, headers: {'Content-type': 'application/json'});


    giftcardresponse loginresult_data =
    giftcardresponse.fromJson(jsonDecode(response));
    return loginresult_data;
  }
}
