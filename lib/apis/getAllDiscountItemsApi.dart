import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getalldiscountitemresponse.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class GetAllDiscountsItemRepository {
  Future<List<Discounts>> getalldiscountitems(
      String itemId,
      String userId,
      String restaurantId,
      ) async {

    var body = json.encode({
      'itemId': itemId,
      'userId': userId,
      'restaurantId': restaurantId
    });
    print(body);
    dynamic response = await Requests.post(base_url + "get_all_disounts_item",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['discounts'];
    return list.map((model) => Discounts.fromJson(model)).toList();

  }

}
