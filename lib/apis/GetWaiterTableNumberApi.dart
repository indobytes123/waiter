import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:waiter/model/getalldinningoptionsresponse.dart';
import 'package:waiter/model/getwaiterservicearearesponse.dart';
import 'package:waiter/model/getwaitertablenumberresponse.dart';
import 'package:waiter/model/loginresponse.dart';
import 'package:waiter/model/waitergenerateotp.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:http/http.dart' as http;
class GetWaiterTableNumberApiRepository {

  Future<List<TableSetupDetails>> getWaiterTableNumber(String restaurantId,String serviceAreaId) async {
    var body = json.encode({'restaurantId':restaurantId,'serviceAreaId':serviceAreaId});
    print(base_url + "get_table_numbers"+ body.toString());

    dynamic response = await Requests.post(base_url + "get_waiter_table_setup",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "WAITERTABLENUMBERSRESPONSE"+ response.toString());
    final res = json.decode(response);
    Iterable list = res['tableSetupDetails'];
    return list.map((model) => TableSetupDetails.fromJson(model)).toList();
  }
}

