import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:waiter/utils/all_constans.dart';

import 'cart.dart';


class YourAppbarBackArrow extends StatelessWidget implements PreferredSizeWidget {

  final int count;
  final String title_text;

  const YourAppbarBackArrow({
    Key key,
    this.count,
    this.title_text,
  }) : super(key: key);
  @override
  Size get preferredSize =>  Size.fromHeight(70);

  @override
  Widget build(BuildContext context) {

    return
      Container(color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            AppBar(
              toolbarHeight:  preferredSize.height,
              automaticallyImplyLeading: false,
              elevation: 0.0,
              backgroundColor: Colors.white,
              centerTitle: false,
              title: Text(
                  title_text,style: new TextStyle(
                  color: login_passcode_text,
                  fontSize: 20.0,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600)

              ),
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    padding: EdgeInsets.only(left: 12.0),
                    icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                },
              ),
              actions: [
                new Padding(
                  padding: const EdgeInsets.only(right: 15),
                  child: new Container(
                      alignment: Alignment.centerRight,
                      height: 48.0,
                      width: 48.0,
                      child: new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,MaterialPageRoute(builder: (context) => Cart()));
                        },
                        child: new Stack(
                          children: <Widget>[
                            Image.asset("images/cart_icon.png"),
                            1 == 0
                                ? new Container()
                                : new Positioned(
                                top: -2.0,
                                right: 0.0,
                                child: new Stack(
                                  children: <Widget>[
                                    new Icon(Icons.brightness_1,
                                        size: 28.0, color: Colors.lightBlueAccent),
                                    new Positioned(
                                        top: 5.0,
                                        right: 12.0,
                                        child: new Center(
                                          child: new Text(
                                              '$count',
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontSize: 11.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )),
                                  ],
                                )),
                          ],
                        ),
                      )),
                )
              ],
            ),
          ],
        ),

      );
  }
}
