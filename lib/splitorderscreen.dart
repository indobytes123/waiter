import 'dart:convert';
import 'dart:developer' as dev;

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:toast/toast.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/all_constans.dart';

import 'apis/addwaiterorder.dart';
import 'apis/getTableInformationApi.dart';
import 'gettableinformation.dart';
import 'model/gettableinformationresponse.dart';

class SplitScreen extends StatefulWidget {
  final String tableNumber;
  const SplitScreen(this.tableNumber, {Key key}) : super(key: key);
  @override
  _SplitScreenState createState() => _SplitScreenState();
}

class _SplitScreenState extends State<SplitScreen> {
  List<String> categories = ["a", "b", "c", "d"];
  List<SplitItemsList> Split_list1 = new List();
  List<SplitItemsList> Split_list2 = new List();
  List<SplitItemsList> Split_list3 = new List();
  List<SplitItemsList> Split_list4 = new List();

  bool _loading = true;
  var restaurant_id = "";
  var employee_id = "";
  int selected_tab_pos = 0;
  int previous_tab_pos;

  List<List<SplitItemsList>> all_checks = new List();
  List<ItemsList> final_all_checks;
  List<SplitItemsList> selected_checkBoxList = new List();

  List<List<TableInfo>> final_total_info = new List();
  List<TableInfo> selected_total_info;
  List<TableInfo> default_table_info = new List();

  List<String> split_orderids = ["0","0","0","0"];
  List<String> split_orderuniqueids = ["0","0","0","0"];

  @override
  void initState() {
    super.initState();

   /* print("Height"+SizeConfig.screenHeight.toString());
    if(SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800){
      toolbar_height = 56;
      screenheight = 470.0;
      cardview_height = 245.0;
    }
    if(SizeConfig.screenHeight >= 800){
      toolbar_height = 66;
      screenheight = 680.0;
      cardview_height = 450.0;
    }*/

    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];

            print("TABLEINFORMATION" + "TABLENUMBER===" + widget.tableNumber.toString() + " EMPLOYEEID===" + employee_id + " RESTAURANTID====" + restaurant_id);

            Future.delayed(Duration(seconds: 2), () async {
              setState(() {
                GetTableInformationApiRepository().checktableinformation(widget.tableNumber, employee_id, restaurant_id).then((value) {
                  print("TABLEINFORMATION RESPSTATUS  " + value.orderInfo.length.toString()+"--"+value.responseStatus.toString());
                  if (value.responseStatus == 1) {
                    setState(() {
                      for (int t = 0; t < value.orderInfo.length; t++) {
                        split_orderids[t]=value.orderInfo[t].orderId;
                        split_orderuniqueids[t]=value.orderInfo[t].orderUniqueId;


                        for (int g = 0; g < value.orderInfo[t].itemsList.length; g++) {
                          default_table_info.add(value.orderInfo[t]);

                          if (t == 0) {

                            Split_list1.add(SplitItemsList(
                                value.orderInfo[t].itemsList[g].discountAmount,
                                value.orderInfo[t].itemsList[g].discountName,
                                value.orderInfo[t].itemsList[g].discountType,
                                value.orderInfo[t].itemsList[g].discountValue,
                                value.orderInfo[t].itemsList[g].isBogo,
                                value.orderInfo[t].itemsList[g].isCombo,
                                value.orderInfo[t].itemsList[g].itemId,
                                value.orderInfo[t].itemsList[g].itemName,
                                value.orderInfo[t].itemsList[g].itemStatus,
                                value.orderInfo[t].itemsList[g].itemType,
                                value.orderInfo[t].itemsList[g].modifiersList,
                                value.orderInfo[t].itemsList[g].itemTaxesList,
                                value.orderInfo[t].itemsList[g].quantity,
                                value.orderInfo[t].itemsList[g].specialRequestList,
                                value.orderInfo[t].itemsList[g].totalPrice,
                                value.orderInfo[t].itemsList[g].unitPrice,
                                false));
                          }
                          if (t == 1) {

                            Split_list2.add(SplitItemsList(
                                value.orderInfo[t].itemsList[g].discountAmount,
                                value.orderInfo[t].itemsList[g].discountName,
                                value.orderInfo[t].itemsList[g].discountType,
                                value.orderInfo[t].itemsList[g].discountValue,
                                value.orderInfo[t].itemsList[g].isBogo,
                                value.orderInfo[t].itemsList[g].isCombo,
                                value.orderInfo[t].itemsList[g].itemId,
                                value.orderInfo[t].itemsList[g].itemName,
                                value.orderInfo[t].itemsList[g].itemStatus,
                                value.orderInfo[t].itemsList[g].itemType,
                                value.orderInfo[t].itemsList[g].modifiersList,
                                value.orderInfo[t].itemsList[g].itemTaxesList,
                                value.orderInfo[t].itemsList[g].quantity,
                                value.orderInfo[t].itemsList[g].specialRequestList,
                                value.orderInfo[t].itemsList[g].totalPrice,
                                value.orderInfo[t].itemsList[g].unitPrice,
                                false));
                          }
                          if (t == 2) {

                            Split_list3.add(SplitItemsList(
                                value.orderInfo[t].itemsList[g].discountAmount,
                                value.orderInfo[t].itemsList[g].discountName,
                                value.orderInfo[t].itemsList[g].discountType,
                                value.orderInfo[t].itemsList[g].discountValue,
                                value.orderInfo[t].itemsList[g].isBogo,
                                value.orderInfo[t].itemsList[g].isCombo,
                                value.orderInfo[t].itemsList[g].itemId,
                                value.orderInfo[t].itemsList[g].itemName,
                                value.orderInfo[t].itemsList[g].itemStatus,
                                value.orderInfo[t].itemsList[g].itemType,
                                value.orderInfo[t].itemsList[g].modifiersList,
                                value.orderInfo[t].itemsList[g].itemTaxesList,
                                value.orderInfo[t].itemsList[g].quantity,
                                value.orderInfo[t].itemsList[g].specialRequestList,
                                value.orderInfo[t].itemsList[g].totalPrice,
                                value.orderInfo[t].itemsList[g].unitPrice,
                                false));
                          }
                          if (t == 3) {

                            Split_list4.add(SplitItemsList(
                                value.orderInfo[t].itemsList[g].discountAmount,
                                value.orderInfo[t].itemsList[g].discountName,
                                value.orderInfo[t].itemsList[g].discountType,
                                value.orderInfo[t].itemsList[g].discountValue,
                                value.orderInfo[t].itemsList[g].isBogo,
                                value.orderInfo[t].itemsList[g].isCombo,
                                value.orderInfo[t].itemsList[g].itemId,
                                value.orderInfo[t].itemsList[g].itemName,
                                value.orderInfo[t].itemsList[g].itemStatus,
                                value.orderInfo[t].itemsList[g].itemType,
                                value.orderInfo[t].itemsList[g].modifiersList,
                                value.orderInfo[t].itemsList[g].itemTaxesList,
                                value.orderInfo[t].itemsList[g].quantity,
                                value.orderInfo[t].itemsList[g].specialRequestList,
                                value.orderInfo[t].itemsList[g].totalPrice,
                                value.orderInfo[t].itemsList[g].unitPrice,
                                false));
                          }
                        }
                      }
                      all_checks.add(Split_list1);
                      all_checks.add(Split_list2);
                      all_checks.add(Split_list3);
                      all_checks.add(Split_list4);
                      _loading = false;
                      //Toast.show("SUCCESS TABLE INFORMATION", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                    });
                  } else if (value.responseStatus == 3) {
                    setState(() {
                      _loading = false;
                      Toast.show("NoneType' object has no attribute 'id'", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                    });
                  } else if (value.responseStatus == 0) {
                    setState(() {
                      _loading = false;
                      Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                    });
                  }
                });
              });
            });
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext ctxt) {
    return new Scaffold(
      appBar: AppBar(
        toolbarHeight: 56,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text("Order", style: new TextStyle(color: login_passcode_text, fontSize: 18.0, fontWeight: FontWeight.w700)),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              padding: EdgeInsets.only(left: 10.0),
              icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
        actions: [
          Container(
              margin: const EdgeInsets.fromLTRB(15, 15, 24, 15),
              height: 30,
              padding: const EdgeInsets.fromLTRB(10, 3, 10, 3),
              alignment: Alignment.center,
              decoration: BoxDecoration(color: cart_text, borderRadius: BorderRadius.circular(0)),
              child: InkWell(
                  child: Text("Done", style: TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400, color: Colors.white)),
                  onTap: () {
                    //print(all_checks.length);
                    print("split_orderids"+split_orderids.length.toString());

                    final_total_info.clear();
                    selected_total_info = new List();
                    for (int i = 0; i < all_checks.length; i++) {

                      //print(all_checks[i].length);

                      double grand_item_total = 0.0;
                      double tax_item_amount = 0.0;
                      double sub_item_total = 0.0;
                      double discount_item_total = 0.0;
                      final_all_checks = new List();
                      for (int j = 0; j < all_checks[i].length; j++) {

                        final_all_checks.add(ItemsList(all_checks[i][j].discountAmount,
                          all_checks[i][j].discountName,
                          all_checks[i][j].discountType,
                          all_checks[i][j].discountValue,
                          all_checks[i][j].isBogo,
                          all_checks[i][j].isCombo,
                          all_checks[i][j].itemId,
                          all_checks[i][j].itemName,
                          all_checks[i][j].itemStatus,
                          all_checks[i][j].itemType,
                          all_checks[i][j].modifiersList,
                          all_checks[i][j].itemTaxesList,
                          all_checks[i][j].quantity,
                          all_checks[i][j].specialRequestList,
                          all_checks[i][j].totalPrice,
                          all_checks[i][j].unitPrice,));
                        print(final_all_checks.length.toString()+"check " +i.toString() +"--" +all_checks[i][j].itemName +"--" +all_checks[i][j].unitPrice +"--" +all_checks[i][j].modifiersList.length.toString() +"--" +all_checks[i][j].specialRequestList.length.toString());
                        double item_total = double.parse(all_checks[i][j].unitPrice) * all_checks[i][j].quantity;
                        discount_item_total = discount_item_total + double.parse(all_checks[i][j].discountAmount);
                        double m_price_total = 0.0;
                        for (int m = 0; m < all_checks[i][j].modifiersList.length; m++) {
                          m_price_total = m_price_total + all_checks[i][j].modifiersList[m].modifierTotalPrice;
                        }

                        double s_price_total = 0.0;
                        for (int n = 0; n < all_checks[i][j].specialRequestList.length; n++) {
                          s_price_total = s_price_total + all_checks[i][j].specialRequestList[n].requestPrice.toDouble();
                        }


                        for (int n = 0; n < all_checks[i][j].itemTaxesList.length; n++) {
                          tax_item_amount = tax_item_amount + all_checks[i][j].itemTaxesList[n].tax.toDouble();

                        }

                        print("tax_item_amount"+tax_item_amount.toString());

                        double split_grand_item_total = item_total + m_price_total + s_price_total;
                        sub_item_total    = sub_item_total + split_grand_item_total;
                        grand_item_total = tax_item_amount + sub_item_total;
                      }
                      selected_total_info.add(TableInfo(i+1,default_table_info[0].createdBy,default_table_info[0].createdOn,default_table_info[0].creditsUsed,default_table_info[0].customerDetails,default_table_info[0].dineInBehaviour,default_table_info[0].dineInOptionId,default_table_info[0].dineInOptionName,discount_item_total,false,final_all_checks,default_table_info[0].noOfGuest,
                          split_orderids[i],split_orderuniqueids[i],default_table_info[0].orderNumber,default_table_info[0].revenueCenterId,default_table_info[0].serviceAreaId,sub_item_total.toString(),default_table_info[0].tableNumber,default_table_info[0].tableStatus,tax_item_amount.toStringAsFixed(2),default_table_info[0].tipAmount,grand_item_total.toString(),default_table_info[0].paymentStatus,default_table_info[0].receiptNumber));
                    }


                    print(selected_total_info);
                    var body = json.encode({'checks':selected_total_info,"userId":employee_id,"restaurantId":restaurant_id,});
                    dev.log(body);
                    SplitWaiterOrderRepository().splitwaiterorder(body).then((result){
                      print("CARTSAVING"+result.toString());
                      if(result.responseStatus == 0){
                        _loading = false;
                        Toast.show("No Data Found", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                      }else {
                        setState(
                                () {
                              //print("ADDWAITERORDERID"+result.orderId);
                              _loading = false;

                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        GetTableInformation(widget.tableNumber,"")),
                              );
                            }
                        );
                      }
                    });

                  }))
        ],
      ),
      body: _loading
          ? Center(
              child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
            )
          : Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              color: Colors.white,
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                  color: dashboard_bg,
                  child: Column(children: [
                    SizedBox(
                      height: 50,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: 4,
                        shrinkWrap: true,
                        itemBuilder: (context, index) => Container(
                            margin: new EdgeInsets.all(5.0),
                            width: 80,
                            child: RaisedButton(
                                color: selected_tab_pos == index ? cart_text : Colors.white,
                                child: Text(
                                  "Split " + (index + 1).toString(),
                                  style: TextStyle(color: selected_tab_pos == index ? Colors.white : add_food_item_bg),
                                ),
                                onPressed: () {
                                  print(previous_tab_pos.toString() + "Split " + index.toString() + "--" + selected_checkBoxList.length.toString());

                                  //if(selected_checkBoxList.length>0) {
                                  setState(() {
                                    selected_tab_pos = index;
                                    if(default_table_info[selected_tab_pos].paymentStatus==1){
    Toast.show("We cannot add items to this check.Payment Done", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
    }else {
                                      all_checks[selected_tab_pos] = all_checks[selected_tab_pos] + selected_checkBoxList;

                                      for (int i = 0; i < all_checks[selected_tab_pos].length; i++) {
                                        all_checks[selected_tab_pos][i].isCheck = false;
                                      }

                                      for (int i = 0; i < selected_checkBoxList.length; i++) {
                                        print(all_checks[previous_tab_pos].indexOf(selected_checkBoxList[i]));
                                        int remove_pos = all_checks[previous_tab_pos].indexOf(selected_checkBoxList[i]);
                                        all_checks[previous_tab_pos].removeAt(remove_pos);
                                      }
                                      selected_checkBoxList.clear();
                                    }
                                  });
                                }
                                // },
                                )),
                      ),
                    ),
                    Expanded(
                        child: Card(
                            margin: EdgeInsets.only(left: 5, right: 5, top: 10),
                            elevation: 3,
                            child: Container(
                                color: Colors.white,
                                child: ListView.builder(
                                    itemCount: all_checks[selected_tab_pos].length,
                                    itemBuilder: (BuildContext context, int i) {
                                      return new Container(
                                        // padding: new EdgeInsets.all(10.0),
                                        child: Column(
                                          children: <Widget>[
                                            new CheckboxListTile(
                                                controlAffinity: ListTileControlAffinity.leading,
                                                activeColor: cart_text,
                                                dense: false,
                                                //font change
                                                title: new Text(
                                                  all_checks[selected_tab_pos][i].itemName,
                                                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.5),
                                                ),
                                                value: all_checks[selected_tab_pos][i].isCheck,
                                                onChanged: default_table_info[selected_tab_pos].paymentStatus==0?(bool val) {
                                                  itemChange(val, selected_tab_pos, i);
                                                }:null),
                                            Container(
                                              margin: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                              child: Divider(
                                                color: cart_viewline,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }))))
                  ])),
            ),
    );
  }

  void itemChange(bool val, int index, int i) {
    setState(() {
      all_checks[index][i].isCheck = val;
      previous_tab_pos = index;
      if (val == true) {
        selected_checkBoxList.add(all_checks[index][i]);
      } else {
        selected_checkBoxList.remove(all_checks[index][i]);
      }
    });
  }
}

class SplitItemsList {
  String discountAmount;
  String discountName;
  String discountType;
  String discountValue;
  bool isBogo;
  bool isCombo;
  String itemId;
  String itemName;
  int itemStatus;
  String itemType;
  List<ModifiersList> modifiersList;
  List<ItemTaxRates> itemTaxesList;
  int quantity;
  List<SpecialRequestList> specialRequestList;
  String totalPrice;
  String unitPrice;
  bool isCheck;

  SplitItemsList(this.discountAmount, this.discountName, this.discountType, this.discountValue, this.isBogo, this.isCombo, this.itemId, this.itemName, this.itemStatus, this.itemType,
      this.modifiersList,this.itemTaxesList, this.quantity, this.specialRequestList, this.totalPrice, this.unitPrice, this.isCheck);

  SplitItemsList.fromJson(Map<String, dynamic> json) {
    discountAmount = json['discountAmount'] as String;
    discountName = json['discountName'];
    discountType = json['discountType'];
    discountValue = json['discountValue'] as String;
    isBogo = json['isBogo'];
    isCombo = json['isCombo'];
    itemId = json['itemId'];
    itemName = json['itemName'];
    itemStatus = json['itemStatus'];
    itemType = json['itemType'];
    if (json['modifiersList'] != null) {
      modifiersList = new List<ModifiersList>();
      json['modifiersList'].forEach((v) {
        modifiersList.add(new ModifiersList.fromJson(v));
      });
    }
    if (json['itemTaxesList'] != null) {
      itemTaxesList = new List<ItemTaxRates>();
      json['itemTaxesList'].forEach((v) {
        itemTaxesList.add(new ItemTaxRates.fromJson(v));
      });
    }
    quantity = json['quantity'];
    if (json['specialRequestList'] != null) {
      specialRequestList = new List<SpecialRequestList>();
      json['specialRequestList'].forEach((v) {
        specialRequestList.add(new SpecialRequestList.fromJson(v));
      });
    }
    totalPrice = json['totalPrice'] as String;
    unitPrice = json['unitPrice'] as String;
    //isCheck = json['unitPrice'] as bool;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['discountAmount'] = this.discountAmount;
    data['discountName'] = this.discountName;
    data['discountType'] = this.discountType;
    data['discountValue'] = this.discountValue;
    data['isBogo'] = this.isBogo;
    data['isCombo'] = this.isCombo;
    data['itemId'] = this.itemId;
    data['itemName'] = this.itemName;
    data['itemStatus'] = this.itemStatus;
    data['itemType'] = this.itemType;
    if (this.modifiersList != null) {
      data['modifiersList'] = this.modifiersList.map((v) => v.toJson()).toList();
    }
    if (this.itemTaxesList != null) {
      data['itemTaxesList'] = this.itemTaxesList.map((v) => v.toJson()).toList();
    }
    data['quantity'] = this.quantity;
    if (this.specialRequestList != null) {
      data['specialRequestList'] = this.specialRequestList.map((v) => v.toJson()).toList();
    }
    data['totalPrice'] = this.totalPrice;
    data['unitPrice'] = this.unitPrice;
    return data;
  }
}

Route _createRoute_gettable(String tableno) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => GetTableInformation("",tableno),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
