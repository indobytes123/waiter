import 'dart:developer';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:regexed_validator/regexed_validator.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/getEditProfileApi.dart';
import 'package:waiter/payment2.dart';
import 'package:waiter/receipt.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/splitorderscreen.dart';
import 'package:waiter/utils/DialogClass.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'addfood_screen.dart';
import 'apis/getProfileApi.dart';
import 'apis/getTableInformationApi.dart';
import 'model/gettableinformationresponse.dart';

class EditProfile extends StatefulWidget {

  final String first_name;
  final String last_name;

  const EditProfile(this.first_name, this.last_name, {Key key})
      : super(key: key);
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  GlobalKey<PageContainerState> key = GlobalKey();
  final _formKey = GlobalKey<FormState>();
  int selected_pos = -1;
  List<TableInfo> __cart_items_list = new List();
  int split_length = 0;
  var cart_count_value = 1;
  int cart_count = 0;
  var table_number = "";
  var order_id = "";
  var no_of_guest = "";
  var restaurant_id = "";
  var employee_id = "";
  double screenheight = 0.0;
  double cardview_height = 0.0;
  double toolbar_height = 0.0;
  bool _loading = true;
  TextEditingController passwordController = TextEditingController();
  TextEditingController _currentpasswordController = TextEditingController();
  TextEditingController confirmpasswordController = TextEditingController();
  TextEditingController username_Controller = TextEditingController();
  CancelableOperation cancelableOperation;
  String _passwordError;
  var user_name = "";

  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());




    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 580 && SizeConfig.screenHeight < 800) {
      toolbar_height = 56;
      screenheight = 460.0;
      cardview_height = 245.0;
    }
    if (SizeConfig.screenHeight >= 800) {
      toolbar_height = 66;
      screenheight = 680.0;
      cardview_height = 450.0;
    }
    UserRepository().getuserdetails().then((userdetails) {
      print("userdata" + userdetails.length.toString());
      setState(() {
        employee_id = userdetails[0];
        //user_id = userdetails[1];
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            restaurant_id = restaurantdetails[0];

            Future.delayed(Duration(seconds: 2), () async {
              setState(() {

                GetProfileApiRepository().getprofiledata(employee_id,restaurant_id).then((value){
                  print("TABLEINFORMATION RESPSTATUS  "+value.responseStatus.toString());
                  debugPrint(base_url + ""+ value.result.toString());
                  if(value.responseStatus == 1){
                    setState(
                            () {
                          _loading = false;
                          //split_length = value.tableInfo.length;
                          user_name = value.profileDict.firstName+""+value.profileDict.lastName;
                          //first_name = value.profileDict.firstName;
                          //last_name = value.profileDict.lastName;
                          //Toast.show("SUCCESS TABLE INFORMATION", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        }
                    );
                  }else if(value.responseStatus == 3){
                    setState(
                            (){
                          _loading = false;
                          Toast.show("NoneType' object has no attribute 'id'", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                        });


                  } else if(value.responseStatus == 0){
                    setState(() {
                      _loading = false;
                      Toast.show("Invalid Credentials", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                    });
                  }
                });
              });
            });
          });
        });
      });
    });
  }

  void refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     /* bottomSheet: Container(
          margin: EdgeInsets.fromLTRB(40, 30, 40, 80),
          height: 60,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: login_passcode_bg1,
              borderRadius: BorderRadius.circular(0)),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                  child: FlatButton(
                      minWidth: double.infinity,
                      height: double.infinity,
                      child: Text("Edit Profile",
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                              color: Colors.white)),
                      onPressed: () {

                      })))),*/
      appBar: AppBar(
        toolbarHeight: toolbar_height,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text("Edit Profile",
            style: new TextStyle(
                color: login_passcode_text,
                fontSize: 18.0,
                fontWeight: FontWeight.w700)),
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              padding: EdgeInsets.only(left: 10.0),
              icon: Image.asset("images/back_arrow.png", width: 22, height: 22),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body:Container(
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          color: Colors.white,
          child: Container(
            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            color: dashboard_bg,
            child:Column(children: [Container(
                constraints: new BoxConstraints.expand(
                  height: 300.0,
                ),
                padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                decoration: new BoxDecoration(
                  //color: add_food_item_bg,
                  color:Color(0xFF003E4B).withOpacity(1.0),
                  image: new DecorationImage(
                    image: new AssetImage('images/man_profile.png'),
                    fit: BoxFit.fitHeight,
                  ),
                ),
                child: new Container(alignment:Alignment.center,child: Stack(
                  children: <Widget>[
                    Image.asset(
                      'images/camera.png',
                      height: 50,
                      width: 50,
                    )
                    ,

                  ],
                ))
            ), Expanded(child: _username_widget())],)
          )),
    );
  }

  Widget _username_widget() {
    return SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.fromLTRB(10.00, 30.00, 10.00, 0.00),
            child: Column(children: [
              Container(
                child: new Form(
                  key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: new Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Username';
                                  return null;
                                },
                                controller: username_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: user_name,
                                    hintStyle: TextStyle(color: login_form_hint, fontSize: SizeConfig.blockSizeHorizontal *2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),


                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Current Password';
                                  if(isPasswordCompliant(val) == false) return '1 uppercase 1 smallercase 1 digit 1 letter ';
                                  return null;
                                },
                                controller: _currentpasswordController,
                                obscureText: true,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Current Password",
                                    hintStyle: TextStyle(color: login_form_hint, fontSize: SizeConfig.blockSizeHorizontal *2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),


                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                onChanged: (val) =>  val,
                                /*validator:  (val) {
                                  if (val.isEmpty) return 'Enter New Password';
                                  return null;
                                },*/
                                validator:(val){
                                  print("PasswordVALIDATION"+validator.password(val).toString()+"====="+ isPasswordCompliant(val).toString());
                                  if(val.isEmpty) return 'Enter New Password';
                                  if(isPasswordCompliant(val) == false) return '1 uppercase 1 smallercase 1 digit 1 letter ';
                                  return null;
                                  /*MultiValidator([

                                  RequiredValidator(errorText: "* Required"),
                                  MinLengthValidator(6,
                                      errorText: "Password should be atleast 6 characters"),
                                  MaxLengthValidator(15,
                                      errorText:
                                      "Password should not be greater than 15 characters")
                                ]);*/},
                                controller: passwordController,
                                obscureText: true,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "New Password",
                                    hintStyle: TextStyle(color: login_form_hint, fontSize: SizeConfig.blockSizeHorizontal *2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),

                              SizedBox(
                                height: 5.0,
                              ),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Confirm Password';
                                  if(isPasswordCompliant(val) == false) return '1 uppercase 1 smallercase 1 digit 1 letter ';
                                  if(val != passwordController.text)
                                    return 'ConfirmPassword Not match';
                                  return null;
                                },
                                controller: confirmpasswordController,
                                obscureText: true,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Confirm Password",
                                    hintStyle: TextStyle(color: login_form_hint, fontSize: SizeConfig.blockSizeHorizontal *2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),
                            ],
                          )),
                      Container(
                          margin: EdgeInsets.fromLTRB(25, 25, 25, 0),
                          height: 50,
                          width: double.infinity,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                          child: InkWell(
                              child: FlatButton(
                                  minWidth: double.infinity,
                                  height: double.infinity,
                                  child: Text("Save Changes", style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                      // SignDialogs.showLoadingDialog(context, "Signing in", _keyLoader);
                                      cancelableOperation?.cancel();
                                      CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                        print("EDITPROFILEUPDATE==="+employee_id+"====="+restaurant_id+"====="+widget.first_name+"====="+widget.last_name+"====="+_currentpasswordController.text.toString()+"======"+passwordController.text.toString()+"======"+confirmpasswordController.text.toString());
                                        GetEditProfileApiRepository().editprofile(employee_id,restaurant_id,widget.first_name,widget.last_name,_currentpasswordController.text.toString(),passwordController.text.toString(),confirmpasswordController.text.toString()).then((value){
                                          print("TABLEINFORMATION RESPSTATUS  "+value.responseStatus.toString());
                                          debugPrint(base_url + ""+ value.result.toString());
                                          if(value.responseStatus == 1){
                                            setState(
                                                    () {
                                                  _loading = false;
                                                  //split_length = value.tableInfo.length;
                                                  Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                                                }
                                            );
                                          }else if(value.responseStatus == 3){
                                            setState(
                                                    (){
                                                  _loading = false;
                                                  Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                                                });


                                          } else if(value.responseStatus == 0){
                                            setState(() {
                                              _loading = false;
                                              Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                                            });
                                          }
                                        });


                                      }));
                                    }
                                  })))
                    ],
                  ),
                ),
              ),
            ])));
  }

  bool validateStructure(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  bool isPasswordCompliant(String password, [int minLength = 6]) {
    if (password == null || password.isEmpty) {
      return false;
    }
    bool hasUppercase = password.contains(new RegExp(r'[A-Z]'));
    bool hasDigits = password.contains(new RegExp(r'[0-9]'));
    bool hasLowercase = password.contains(new RegExp(r'[a-z]'));
    bool hasSpecialCharacters = password.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'));
    bool hasMinLength = password.length > minLength;

    return hasDigits & hasUppercase & hasLowercase & hasSpecialCharacters & hasMinLength;
  }


  validate() {
    if(!validateStructure(passwordController.text)){
      setState(() {
        _passwordError = "Enter Valid Password";
      });
      // show dialog/snackbar to get user attention.
      return;
    }
  }

}



class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }




}







Route _createRoute(String table_no) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) =>
        SplitScreen(table_no),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
