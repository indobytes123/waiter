import 'dart:convert';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:waiter/addfooditems_screen.dart';
import 'package:waiter/model/get_all_bogo_model.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/Globals.dart';

import 'addbogoitems_screen.dart';
import 'addcomboitems_screen.dart';
import 'apis/CartsRepository.dart';
import 'apis/getallmenuapi.dart';
import 'apis/getmenugroup.dart';
import 'appbar_back_arrow.dart';
import 'model/get_all_combo_model.dart';
import 'model/getallmenus.dart';
import 'model/getmenugroups.dart';

class AddFood extends StatefulWidget {
  final header_value;
  final table_number;
  final no_of_guests;

  const AddFood(this.header_value, this.table_number, this.no_of_guests,
      {Key key})
      : super(key: key);

  @override
  _AddFoodState createState() => _AddFoodState();
}

class _AddFoodState extends State<AddFood> {
  List<MenuList> _get_all_menus = new List();
  List<MenugroupsList> _get_menu_groups = new List();
  bool _loading = true;
  int cart_count = 0;
  double total_qty = 0.0;

  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  int selected_pos = 0;
  String userid = "";
  String table_number_session = "";
  String no_of_guests_session = "";
  String res_id = "";
  String menu_id = "";
  List<Discounts> _get_all_combos = new List();
  List<DiscountsBogo> get_all_bogo = new List();

  @override
  void initState() {
    super.initState();

    print("HEADERVALUE " + widget.header_value.toString());
    UserRepository().getuserdetails().then((userdetails) {
      setState(() {
        userid = userdetails[0];
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            res_id = restaurantdetails[0];
            print("restaurant_id" + res_id);
            UserRepository()
                .getGenerateTablenumbernGuests()
                .then((tableservicedetails) {
              setState(() {
                table_number_session = tableservicedetails[0];
                no_of_guests_session = tableservicedetails[1];
                print("ADDFOODSCREENTABLE" +
                    "----" +
                    table_number_session +
                    "-----" +
                    no_of_guests_session);
                GetAllMenu().getmenu(userid, res_id).then((result_allmenus) {
                  setState(() {
                    //isLoading = true;
                    _get_all_menus = result_allmenus;
                    menu_id = _get_all_menus[0].id;
                    print(_get_all_menus.length);
                    GetMenuGroups()
                        .getmenu(userid, res_id, menu_id)
                        .then((result_allmenus) {
                      setState(() {
                        //isLoading = true;
                        _get_menu_groups = result_allmenus;
                        print(_get_menu_groups.length);
                        _loading = false;
                        CartsRepository().getcartslisting().then((cartList) {
                          setState(() {
                            cart_count = cartList.length;
                            print("ADDFOODSCREEN" +
                                cart_count.toString() +
                                "----" +
                                widget.table_number +
                                "-----" +
                                widget.no_of_guests);
                            GetAllCombo()
                                .getcombo(userid, res_id)
                                .then((result_allcomboitems) {
                              setState(() {
                                //isLoading = true;
                                _get_all_combos = result_allcomboitems;
                                print("COMBOSIZE" +
                                    _get_all_combos.length.toString());
                                _loading = false;
                                GetAllBogo()
                                    .getbogo(userid, res_id)
                                    .then((result_allbogoitems) {
                                  setState(() {
                                    //isLoading = true;
                                    get_all_bogo = result_allbogoitems;
                                    print("BOGOSIZE" +
                                        get_all_bogo.length.toString());
                                    _loading = false;
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: YourAppbarBackArrow(
          count: cart_count,
          title_text: "Adding Food",
        ),
        body: _loading
            ? Center(
                child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
              )
            : Container(
                color: Colors.white,
                child: Column(
                  children: [
                    widget.header_value == true
                        ? Container(
                            color: dashboard_bg,
                            margin: EdgeInsets.only(left: 15, right: 15),
                            padding: EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 12.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Image.asset(
                                      "images/round_table.png",
                                      height: 48,
                                      width: 48,
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: 10, bottom: 0),
                                      child: RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                            text: "Table no\n",
                                            style: TextStyle(
                                                color: add_food_item_bg,
                                                fontSize: 14,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400)),
                                        TextSpan(
                                            text: widget.table_number,
                                            style: new TextStyle(
                                                fontSize: 18,
                                                color: add_food_item_bg,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w700))
                                      ])),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Image.asset(
                                      "images/round_customers_icon.png",
                                      height: 48,
                                      width: 48,
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: 10, bottom: 0),
                                      child: RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                            text: "Customers\n",
                                            style: TextStyle(
                                                color: add_food_item_bg,
                                                fontSize: 14,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400)),
                                        TextSpan(
                                            text: widget.no_of_guests,
                                            style: new TextStyle(
                                                fontSize: 18,
                                                color: add_food_item_bg,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w700))
                                      ])),
                                    ),
                                  ],
                                )
                              ],
                            ))
                        : SizedBox(),
                    SizedBox(height: 15),
                    /*get_all_bogo.length > 0? */ _get_all_combos.length > 0?
                    Container(
                        color: add_food_item_bg,
                        margin: EdgeInsets.only(left: 10, right: 10),
                        padding: EdgeInsets.symmetric(
                            horizontal: 12.0, vertical: 12.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            /*_get_all_combos.length > 0
                                ?*/ InkWell(
                                    child: Expanded(
                                        child: Container(
                                            alignment: Alignment.centerLeft,
                                            color: Colors.white,
                                            margin: EdgeInsets.only(right: 0),
                                            height: 48,
                                            child: Row(
                                              //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                              children: <Widget>[
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 12, bottom: 0),
                                                    child: new Image.asset(
                                                      "images/appetizer.png",
                                                      height: 28,
                                                      width: 28,
                                                    )),
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 12, bottom: 0),
                                                  child: Text(
                                                    "Combos",
                                                    style: TextStyle(
                                                        color: add_food_item_bg,
                                                        fontSize: 16,
                                                        fontFamily: 'Poppins',
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 15, right: 15),
                                                    child: Image.asset(
                                                      'images/next.png',
                                                      height: 15,
                                                      width: 20,
                                                    )),
                                              ],
                                            ))),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AddComboItem("Combos", "", "",
                                                      "", "", true)));
                                    },
                                  )
                               /* : SizedBox()*/,
                            /*get_all_bogo.length > 0
                                ?*/ InkWell(
                                    child: Expanded(
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            color: Colors.white,
                                            margin: EdgeInsets.only(left: 0),
                                            height: 48,
                                            child: Row(
                                              //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                              children: <Widget>[
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 12, bottom: 0),
                                                    child: new Image.asset(
                                                      "images/appetizer.png",
                                                      height: 28,
                                                      width: 28,
                                                    )),
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 12, bottom: 0),
                                                  child: Text(
                                                    "Bogos",
                                                    style: TextStyle(
                                                        color: add_food_item_bg,
                                                        fontSize: 16,
                                                        fontFamily: 'Poppins',
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                                Container(
                                                    alignment:
                                                        Alignment.centerRight,
                                                    padding: EdgeInsets.only(
                                                        left: 15, right: 15),
                                                    child: Image.asset(
                                                      'images/next.png',
                                                      height: 15,
                                                      width: 20,
                                                    )),
                                              ],
                                            ))),
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => AddBogoItem(
                                                  "Bogos",
                                                  "",
                                                  "",
                                                  "",
                                                  "",
                                                  true)));
                                    },
                                  )
                               /* : SizedBox()*/
                          ],
                        )) /*:SizedBox()*/ :SizedBox(),
                    SizedBox(height: 0),
                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      padding:
                          EdgeInsets.symmetric(vertical: 20.0, horizontal: 0.0),
                      height: MediaQuery.of(context).size.height * 0.2,
                      color: dashboard_bg,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _get_all_menus.length,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.all(5),
                              width: MediaQuery.of(context).size.width * 0.3,
                              child: InkWell(
                                  /* onTap: () => setState(() => selected_pos = index),*/
                                  onTap: () {
                                    selected_pos = index;
                                    menu_id = _get_all_menus[index].id;
                                    print("MENUID" +
                                        menu_id +
                                        "----" +
                                        res_id +
                                        "----" +
                                        userid);
                                    GetMenuGroups()
                                        .getmenu(userid, res_id, menu_id)
                                        .then((result_allmenus) {
                                      setState(() {
                                        //isLoading = true;
                                        _get_menu_groups = result_allmenus;
                                        print("LENGTH" +
                                            _get_menu_groups.length.toString());
                                      });
                                    });
                                  },
                                  child: Card(
                                    color: selected_pos == index
                                        ? Colors.lightBlueAccent
                                        : add_food_item_bg,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.horizontal(
                                            left: Radius.circular(3.00),
                                            right: Radius.zero),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Image.asset(
                                            'images/food.png',
                                            height: 36,
                                            width: 36,
                                          ),
                                          SizedBox(height: 5),
                                          Text(
                                            _get_all_menus[index]
                                                .menuName
                                                .capitalizeFirstofEach,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.white,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w600,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )),
                            );
                          }),
                    ),
                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          //padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 24.0),
                          height: MediaQuery.of(context).size.height * 0.2,
                          color: dashboard_bg,
                          child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: _get_menu_groups.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                                  width:
                                      MediaQuery.of(context).size.width * 0.3,
                                  child: InkWell(
                                      onTap: () => Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => AddFoodItem(
                                                  _get_menu_groups[index].name,
                                                  menu_id,
                                                  _get_menu_groups[index].id,
                                                  widget.table_number,
                                                  widget.no_of_guests,
                                                  widget.header_value))),
                                      child: Card(
                                        color: Colors.white,
                                        child: Container(
                                          height: 56,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 1,
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 15),
                                                      child: Image.asset(
                                                        'images/appetizer.png',
                                                        height: 26,
                                                        width: 26,
                                                      ))),
                                              Expanded(
                                                flex: 4,
                                                child: Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 10),
                                                    child: Text(
                                                      _get_menu_groups[index]
                                                          .name,
                                                      style: TextStyle(
                                                          color:
                                                              login_passcode_text,
                                                          fontSize: 16,
                                                          fontFamily: 'Poppins',
                                                          fontWeight:
                                                              FontWeight.w600),
                                                      textAlign:
                                                          TextAlign.start,
                                                    )),
                                              ),
                                              Expanded(
                                                  flex: 1,
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                          right: 5),
                                                      child: Image.asset(
                                                        'images/menu_arrow.png',
                                                        height: 16,
                                                        width: 20,
                                                      ))),
                                            ],
                                          ),
                                        ),
                                      )),
                                );
                              })),
                    ),
                  ],
                )));
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
