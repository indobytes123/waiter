import 'dart:convert';
import 'dart:developer' as dev;
import 'dart:math';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:requests/requests.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:waiter/apis/CartsRepository.dart';
import 'package:waiter/model/addcomboitemstatus.dart';
import 'package:waiter/model/get_all_combo_model.dart';
import 'package:waiter/model/get_items_by_menugroupid.dart';
import 'package:waiter/model/get_items_by_menugroupid.dart' as slist;
import 'package:waiter/model/getmenuitemdata.dart';
import 'package:waiter/session/userRepository.dart';
import 'package:waiter/utils/Globals.dart';
import 'package:waiter/utils/all_constans.dart';

import 'appbar_back_arrow.dart';
import 'model/cartmodelitemsapi.dart';

class GetAllCombo {
  Future<List<Discounts>> getcombo(
    String user_id,
    String res_id,
  ) async {
    var body = json.encode({'userId': user_id, 'restaurantId': res_id});
    print(body);
    dynamic response = await Requests.post(base_url + "get_all_combo_disounts", body: body, headers: {'Content-type': 'application/json'});
    dev.log("resultcomboitem---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['discounts'];
      return list.map((model) => Discounts.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class GetItemsbyMenuGroupID {
  Future<List<MenuItems>> getitems(
    String menu_group_id,
    String res_id,
  ) async {
    var body = json.encode({'menuGroupId': menu_group_id, 'restaurantId': res_id});
    print(body);
    dynamic response = await Requests.post(base_url + "get_menu_items_on_menu_group", body: body, headers: {'Content-type': 'application/json'});
    print("resultget_menu_items_on_menu_group---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['menuItems'];
      return list.map((model) => MenuItems.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class GetItemsbyMenuData {
  Future<getmenuitemdata> getitems(
    String menu_id,
    String res_id,
  ) async {
    var body = json.encode({'menuItemId': menu_id, 'restaurantId': res_id});
    print(body);
    dynamic response = await Requests.post(base_url + "get_menu_item_data", body: body, headers: {'Content-type': 'application/json'});
    print("resultmenuItemData---" + response);
    //final res = json.decode(response);
    //print("result---"+res['menuItemData']);
    getmenuitemdata placeorder_data = getmenuitemdata.fromJson(jsonDecode(response));
    //print(placeorder_data.sizeList);
    return placeorder_data;
    /*if (res['responseStatus'] == 1) {
      Iterable list = res['menuItemData'];
      return list.map((model) => MenuItemData.fromJson(model)).toList();
    } else {
      return [];
    }*/
  }
}

class AddComboItem extends StatefulWidget {
  final String menu_name;
  final String menu_id;
  final String menu_groupid;
  final String table_number;
  final String no_of_guests;
  final bool header_value;

  const AddComboItem(this.menu_name, this.menu_id, this.menu_groupid, this.table_number, this.no_of_guests, this.header_value, {Key key})
      : super(key: key);

  @override
  _AddComboItemState createState() => _AddComboItemState();
}

class _AddComboItemState extends State<AddComboItem> {
  int isExpanded = -1;
  List<Discounts> _get_all_combos;

  List<MenuCartItemapi> __cart_items_list = new List();
  List<String> selectedCheckbox = [];
  bool _loading = true;
  String item_id = "";
  String item_menugroup_id = "";
  String item_menu_id = "";
  String item_name = "";
  String table_number_session = "";
  String no_of_guests_session = "";

  String userid = "";
  String res_id = "";
  bool checkvalue = false;
  List<MenuItems> menuitems_bygroup = new List();
  int cart_count = 0;
  List<String> comboid_type;
  List<addcomboitemstatus> comboid_with_types = new List();
  CancelableOperation cancelableOperation;
  String returnVal = "";
  @override
  void initState() {
    super.initState();
    // print(widget.main_order_id.toString());

    UserRepository().getuserdetails().then((userdetails) {
      setState(() {
        userid = userdetails[0];
        //userid = "60893ea9180d0231ab2bbbbf";
        UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
          setState(() {
            res_id = restaurantdetails[0];
            //res_id = "6083c539b054fccb93b9cd82";
            print("FOODITEMS " + widget.menu_id + "--" + widget.menu_groupid + "--" + res_id);
            GetAllCombo().getcombo(userid, res_id).then((result_allcomboitems) {
              setState(() {
                //isLoading = true;
                _get_all_combos = result_allcomboitems;
                dev.log(_get_all_combos.toString());

                UserRepository().getcombo_with_types().then((cartList) {
                  setState(() {
                    List<addcomboitemstatus> get_savedcomboselected = cartList;
                    print("sssss" + get_savedcomboselected.length.toString());

                    for (int g = 0; g < _get_all_combos.length; g++) {
                      print(_get_all_combos[g].comboList);
                      for (int h = 0; h < _get_all_combos[g].comboList.length; h++) {
                        print(_get_all_combos[g].comboList[h].items);
                        comboid_type = new List();
                        if (_get_all_combos[g].comboList[h].items.length > 0) {
                          for (int i = 0; i < _get_all_combos[g].comboList[h].items.length; i++) {
                            print(_get_all_combos[g].comboList[h].items[i].type);
                            comboid_type.add("0");
                          }
                          comboid_with_types.add(addcomboitemstatus(_get_all_combos[g].comboList[h].items[0].comboId, comboid_type));
                        }
                      }
                    }
                    UserRepository().savecombo_with_types(comboid_with_types);
                    // }
                  });
                });

                Future.delayed(Duration(seconds: 2), () async {
                  setState(() {
                    CartsRepository().getcartslisting().then((cartList) {
                      setState(() {
                        __cart_items_list = cartList;
                        cart_count = __cart_items_list.length;

                        UserRepository().getGenerateTablenumbernGuests().then((tableservicedetails) {
                          setState(() {
                            table_number_session = tableservicedetails[0];
                            no_of_guests_session = tableservicedetails[1];
                            print("TABLESERVICEDETAILS" + table_number_session + "---------" + no_of_guests_session);
                          });
                        });

                        //_loading = false;
                      });
                    });
                  });
                });

                _loading = false;
              });
            });
          });
        });

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    /*print(comboid_with_types.length);
    for (int g = 0; g < comboid_with_types.length; g++) {
      print(comboid_with_types[g].comboid);
      var data= comboid_with_types.where((row) => (row.comboid.contains("609271d3208e2b9170a6bec71"))).toList();

      for (var element in data) {
        print(element.menuItemStatus);
        for (int h = 0; h < element.menuItemStatus.length; h++) {
          print(element.menuItemStatus[h]);
        }
      }
    }*/
    //comboid_with_types.ge

    var _crossAxisSpacing = 8;
    var _screenWidth = MediaQuery.of(context).size.width;
    var _crossAxisCount = 2;
    var _width = (_screenWidth - ((_crossAxisCount - 1) * _crossAxisSpacing)) / _crossAxisCount;
    var cellHeight = 60;
    var _aspectRatio = _width / cellHeight;
    return Scaffold(
        appBar: YourAppbarBackArrow(
          count: cart_count,
          title_text: widget.menu_name,
        ),
        body: _loading
            ? Center(
                child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
              )
            : Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Expanded(
                      child: Container(
                          margin: EdgeInsets.only(left: 10, right: 10),
                          padding: EdgeInsets.all(15),
                          //height: MediaQuery.of(context).size.height * 0.2,
                          color: dashboard_bg,
                          child: ListView.separated(
                              separatorBuilder: (context, index) => Divider(
                                    color: dashboard_bg,
                                    height: 10,
                                  ),
                              scrollDirection: Axis.vertical,
                              itemCount: _get_all_combos.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return Card(
                                    elevation: 3,
                                    child: Container(
                                        margin: EdgeInsets.all(0),
                                        width: MediaQuery.of(context).size.width * 0.3,
                                        child: InkWell(
                                          onTap: () {},
                                          child: Container(
                                            color: Colors.white,
                                            //padding: EdgeInsets.all(15),
                                            //height: 250,
                                            child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Expanded(
                                                      child: Padding(
                                                          padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                                                          child: Text(
                                                            _get_all_combos[index].name.toString(),
                                                            style: TextStyle(
                                                                color: Colors.lightBlueAccent,
                                                                fontSize: 16,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w700),
                                                            textAlign: TextAlign.start,
                                                          ))),
                                                  Row(
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.only(
                                                            left: 0,
                                                            right: 0,
                                                          ),
                                                          child: FlatButton(
                                                              minWidth: 30,
                                                              color: kToolbarTitleColor,
                                                              child: Text(
                                                                new String.fromCharCodes(new Runes('\u0024')) +
                                                                    _get_all_combos[index].discountValue.toStringAsFixed(2),
                                                                style: TextStyle(
                                                                    color: add_food_item_bg,
                                                                    fontSize: 16,
                                                                    fontFamily: 'Poppins',
                                                                    fontWeight: FontWeight.w700),
                                                                textAlign: TextAlign.start,
                                                              ),
                                                              onPressed: () {}))
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              ListView.builder(
                                                /* padding: EdgeInsets.only(
                                                left: 5.0, right: 5.0, top: 5, bottom: 5),*/
                                                shrinkWrap: true,
                                                itemCount: _get_all_combos[index].comboList.length,
                                                itemBuilder: (context, c) {
                                                  //final item = searchList[index];
                                                  return _get_all_combos[index].comboList[c].items.length > 0
                                                      ? Container(
                                                          color: dashboard_bg,
                                                          //padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 0, bottom: 0),
                                                          margin: EdgeInsets.all(5),
                                                          child: GridView.builder(
                                                            shrinkWrap: true,
                                                            itemCount: _get_all_combos[index].comboList[c].items.length,
                                                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                                                crossAxisCount: _crossAxisCount, childAspectRatio: _aspectRatio),
                                                            itemBuilder: (context, i) {
                                                              //final item = searchList[index];
                                                              return Container(
                                                                  color: dashboard_bg,
                                                                  padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 0, bottom: 0),
                                                                  margin: EdgeInsets.only(left: 5, right: 5),
                                                                  child: ListTile(
                                                                    title: Text(_get_all_combos[index].comboList[c].items[i].name,
                                                                        style: TextStyle(
                                                                            fontSize: 14,
                                                                            fontFamily: 'Poppins',
                                                                            fontWeight: FontWeight.w600,
                                                                            color: add_food_item_bg)),
                                                                    onTap: () {
                                                                      setState(() {
                                                                        print(_get_all_combos[index].comboList[c].items.length);
                                                                        gewinner(index,c);

                                                                      });
                                                                    },
                                                                  ));
                                                            },
                                                          ),
                                                        )
                                                      : SizedBox();
                                                },
                                              ),
                                              isExpanded == index
                                                  ? new Container(
                                                      child: new ListView.builder(
                                                          shrinkWrap: true,
                                                          scrollDirection: Axis.vertical,
                                                          itemCount: menuitems_bygroup.length,
                                                          itemBuilder: (context, m) {
                                                            return new Column(children: <Widget>[
                                                              new Container(
                                                                height: 50.0,
                                                                color: Colors.green,
                                                                child: Padding(
                                                                    padding: EdgeInsets.only(left: 15),
                                                                    child: Text(
                                                                      menuitems_bygroup[m].itemName,
                                                                      style: TextStyle(
                                                                          color: login_passcode_text,
                                                                          fontSize: 14,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w700),
                                                                      textAlign: TextAlign.start,
                                                                    )),
                                                              )
                                                            ]);
                                                          }))
                                                  : SizedBox(),
                                              SizedBox(height: 15)
                                             /* Padding(
                                                  padding: EdgeInsets.only(
                                                    left: 0,
                                                    right: 0,
                                                  ),
                                                  child: InkWell(
                                                    child: FlatButton(
                                                        minWidth: 30,
                                                        height: 30,
                                                        color: Colors.lightBlueAccent,
                                                        child: Text("Add",
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w600,
                                                                color: add_food_item_bg)),
                                                        onPressed: () {}),
                                                  ))*/
                                            ]),
                                          ),
                                        )));
                              })),
                    ),
                  ],
                )));
  }
  Future gewinner(int index,int cpos) async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
              comboprice: _get_all_combos[index].discountValue.toString(),
              all_comboitems_list: _get_all_combos[index].comboList[cpos].items);
        });
    print("RETURNVALUEINSIDE" + returnVal);

    if (returnVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {
          CartsRepository().getcartslisting().then((cartList) {
            setState(() {
              __cart_items_list = cartList;
              cart_count = __cart_items_list.length;
              print(cart_count);
              final mapped = __cart_items_list.fold<Map<String, Map<String, dynamic>>>({}, (p, v) {
                final name = v.itemId;
                if (p.containsKey(name)) {
                  p[name]["NumberOfItems"] += int.parse(v.quantity);
                } else {
                  p[name] = {
                    "NumberOfItems": int.parse(v.quantity)
                  };
                }
                return p;
              });
              print(mapped.length);
              print(mapped);
           //_loading = false;
            });
          });
        });
      });
    }
  }
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.comboprice,
    this.all_comboitems_list,
  });

  final List<Items> all_comboitems_list;
  final String comboprice;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  List<MenuItems> comboitems_arraylist = new List();
  List<List<MenuItems>> items_arraymenulist = new List();
  List<List<int>> totalsizelist_selectpos = new List();
  List<int> sizelist_selectpos;
  List<addcomboitemstatus> get_savedcomboselected = new List();
  String default_sizeitem = "";
  String res_id = "";
  int item_index_pos = 0;
  bool _loading = true;
  List<MenuCartItemapi> addselecteditemwithsize = new List();
  String combo_uniqueid;
  @override
  void initState() {
    super.initState();
    var random = Random();
    var n1 = random.nextInt(9999);
    combo_uniqueid = "combo_"+n1.toString();
    print("random"+n1.toString());
    UserRepository().getGenerateOtpDetails().then((restaurantdetails) {
      setState(() {
        res_id = restaurantdetails[0];
    print(widget.all_comboitems_list[0].comboId);
    for (int g = 0; g < widget.all_comboitems_list.length; g++) {
      //print(widget.all_comboitems_list[g].comboId);

      if (widget.all_comboitems_list[g].type == "Menu Group") {
        //print(widget.all_comboitems_list[g].type);

        GetItemsbyMenuGroupID().getitems(widget.all_comboitems_list[g].id, res_id).then((result_allmenuitems) {
          setState(() {
            //print(result_allmenuitems);
            comboitems_arraylist = result_allmenuitems;
            items_arraymenulist.add(comboitems_arraylist);
            sizelist_selectpos = new List();
            for (int h = 0; h < comboitems_arraylist.length; h++) {
              if(h==0){
                String item_size = "";
                if(comboitems_arraylist[h].sizeList.length>0){
                  item_size = comboitems_arraylist[h].sizeList[0].sizeName;
                }else {
                  item_size = "";
                }
                addselecteditemwithsize.add(MenuCartItemapi(
                    comboitems_arraylist[h].itemId,
                    comboitems_arraylist[h].menuId,
                    comboitems_arraylist[h].menuGroupId,
                    comboitems_arraylist[h].itemName+" "+item_size,
                    "0",
                    "1",
                    "0",
                    "",
                    [],
                    [],
                    comboitems_arraylist[h].itemType,
                    comboitems_arraylist[h].discountId,
                    comboitems_arraylist[h].discountName,
                    "",
                    comboitems_arraylist[h].discountType,
                    comboitems_arraylist[h].discountValue,
                    double.parse(comboitems_arraylist[h].discountAmount.toString()),
                    widget.all_comboitems_list[g].comboId,
                    combo_uniqueid,
                    true,
                    "",
                    "",
                    false,
                    false,
                    false,
                    []));
              }

              if (comboitems_arraylist[h].sizeList.length > 0) {
                for (int i = 0; i < comboitems_arraylist[h].sizeList.length; i++) {
                  if (i == 0) {
                    sizelist_selectpos.add(0);
                  }
                }
              } else {
                //print(h.toString() + "else");
                sizelist_selectpos.add(-1);
              }
            }
            //print("abcddd---"+sizelist_selectpos.length.toString());
            totalsizelist_selectpos.add(sizelist_selectpos);

            _loading = false;
          });
        });
      }
      else {
        //print(widget.all_comboitems_list[g].type + "--" + widget.all_comboitems_list[g].id);
        GetItemsbyMenuData().getitems(widget.all_comboitems_list[g].id, res_id).then((resultitems) {
          setState(() {
            MenuItemData result_allmenuitems = resultitems.menuItemData;
            //print("result----" + result_allmenuitems.toString());
            //print("Menu Itesm size" + result_allmenuitems.sizeList.length.toString());
            List<slist.SizeList> test = new List();
            for (int s = 0; s < result_allmenuitems.sizeList.length; s++) {
              test.add(slist.SizeList(result_allmenuitems.sizeList[s].price, result_allmenuitems.sizeList[s].sizeName));
            }

            comboitems_arraylist.add(MenuItems(
                "0.0",
                result_allmenuitems.diningOptionTaxException,
                result_allmenuitems.diningTaxOption,
                result_allmenuitems.discountAmount,
                result_allmenuitems.discountId,
                result_allmenuitems.discountName,
                result_allmenuitems.discountType,
                result_allmenuitems.discountValue,
                result_allmenuitems.isBogo,
                result_allmenuitems.isCombo,
                result_allmenuitems.itemId,
                result_allmenuitems.itemName,
                result_allmenuitems.itemType,
                result_allmenuitems.menuGroupId,
                result_allmenuitems.menuId,
                result_allmenuitems.pricingStrategy,
                result_allmenuitems.quantity,
                test,
                result_allmenuitems.taxIncludeOption,
                result_allmenuitems.type));
            items_arraymenulist.add(comboitems_arraylist);
            sizelist_selectpos = new List();
            for (int h = 0; h < comboitems_arraylist.length; h++) {
              if(h==0){
                String item_size = "";
                if(comboitems_arraylist[h].sizeList.length>0){
                  item_size = comboitems_arraylist[h].sizeList[0].sizeName;
                }else {
                  item_size = "";
                }
                addselecteditemwithsize.add(MenuCartItemapi(
                    comboitems_arraylist[h].itemId,
                    comboitems_arraylist[h].menuId,
                    comboitems_arraylist[h].menuGroupId,
                    comboitems_arraylist[h].itemName+" "+item_size,
                    "0",
                    "1",
                    "0",
                    "",
                    [],
                    [],
                    comboitems_arraylist[h].itemType,
                    comboitems_arraylist[h].discountId,
                    comboitems_arraylist[h].discountName,
                    "",
                    comboitems_arraylist[h].discountType,
                    comboitems_arraylist[h].discountValue,
                    double.parse(comboitems_arraylist[h].discountAmount.toString()),
                    widget.all_comboitems_list[g].comboId,
                    combo_uniqueid,
                    true,
                    "",
                    "",
                    false,
                    false,
                    false,
                    []));
              }
              //print(h.toString() + "chaitanya" + comboitems_arraylist[h].sizeList.length.toString());
              if (comboitems_arraylist[h].sizeList.length > 0) {
                for (int i = 0; i < comboitems_arraylist[h].sizeList.length; i++) {
                  if (i == 0) {
                    sizelist_selectpos.add(0);
                  }
                }
              } else {
                sizelist_selectpos.add(-1);
              }
            }
            //print("abcddd---"+sizelist_selectpos.length.toString());
            totalsizelist_selectpos.add(sizelist_selectpos);

            //print("tetstttt---"+totalsizelist_selectpos[0].length.toString());
            //print("tetstt---"+totalsizelist_selectpos[0][0].toString());
          });
        });
      }
    }
      });
    });
    UserRepository().getcombo_with_types().then((cartList) {
      setState(() {
        get_savedcomboselected = cartList;
        //print("sirirni" + get_savedcomboselected.length.toString());
        item_index_pos = get_savedcomboselected.indexWhere((element) => element.comboid == widget.all_comboitems_list[0].comboId);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.white,
        insetPadding: EdgeInsets.all(20),
        child: Column(children: [
          Expanded(
              child: SingleChildScrollView(
                  child: Container(
                      //height: MediaQuery.of(context).size.height,
                      child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                  //padding: EdgeInsets.all(5),
                  //height: MediaQuery.of(context).size.height * 0.2,
                  color: dashboard_bg,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: widget.all_comboitems_list.length,
                        itemBuilder: (BuildContext context1, index) {
                          print(widget.all_comboitems_list[index].type);
                          return Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                            child: Column(
                              children: [
                                Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.only(left: 5, bottom: 10),
                                        child: Text(
                                          widget.all_comboitems_list[index].name,
                                          style:
                                              TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        )),

                                  ],
                                ),
                                _loading
                                    ? Center(
                                  child: SpinKitFadingCircle(color: Colors.lightBlueAccent),
                                )
                                    :ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: items_arraymenulist[index].length,
                                    itemBuilder: (BuildContext context1, j) {
                                      return Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                  flex: 3,
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                        left: 0,
                                                        right: 5,
                                                      ),
                                                      child: Text(
                                                        items_arraymenulist[index][j].itemName.toString(),
                                                        style: TextStyle(
                                                            color: login_passcode_text,
                                                            fontSize: 16,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w500),
                                                        textAlign: TextAlign.left,
                                                      ))),
                                              get_savedcomboselected[item_index_pos].menuItemStatus[index] == j.toString()?IconButton(
                                                padding: EdgeInsets.only(left: 0.0),
                                                icon: Image.asset("images/check.png", width: 24, height: 24),
                                                onPressed: () {
                                                  Scaffold.of(context).openDrawer();
                                                },
                                              ):Expanded(
                                                  flex: 1,
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                        left: 0,
                                                        right: 5,
                                                      ),
                                                      child: FlatButton(
                                                        //minWidth: 30,
                                                          color: Colors.lightBlueAccent,
                                                          child: Text("Add",
                                                              style: TextStyle(
                                                                  fontSize: 14,
                                                                  fontFamily: 'Poppins',
                                                                  fontWeight: FontWeight.w800,
                                                                  color: add_food_item_bg)),
                                                          onPressed: () {
                                                            print(j.toString() + "jjjjj" + index.toString());
                                                            setState(() {
                                                              get_savedcomboselected[item_index_pos].menuItemStatus[index] = j.toString();
                                                              UserRepository().savecombo_with_types(get_savedcomboselected);
                                                            });

                                                            String _itemname = "";
                                                            if (totalsizelist_selectpos[index][j] == -1) {
                                                              //print("bcbccggcgc"+"--"+items_arraymenulist[index][j].itemName);
                                                              _itemname = items_arraymenulist[index][j].itemName;
                                                            } else {
                                                              //print(totalsizelist_selectpos[index][j].toString()+"--"+items_arraymenulist[index][j].itemName+"--"+items_arraymenulist[index][j].sizeList[totalsizelist_selectpos[index][j]].sizeName);
                                                              _itemname = items_arraymenulist[index][j].itemName +
                                                                  " " +
                                                                  items_arraymenulist[index][j].sizeList[totalsizelist_selectpos[index][j]].sizeName;
                                                            }

                                                            print(widget.all_comboitems_list[index]);
                                                            print(_itemname);

                                                            addselecteditemwithsize[index].itemId = items_arraymenulist[index][j].itemId;
                                                            addselecteditemwithsize[index].menuId = items_arraymenulist[index][j].menuId;
                                                            addselecteditemwithsize[index].menuGroupId = items_arraymenulist[index][j].menuGroupId;
                                                            addselecteditemwithsize[index].itemName = _itemname;


                                                          })))
                                            ],
                                          ),
                                          items_arraymenulist[index][j].sizeList.length > 0
                                              ? Container(
                                                  margin: EdgeInsets.only(left: 0, top: 5, right: 10),
                                                  //padding: EdgeInsets.all(5),
                                                  height: MediaQuery.of(context).size.height * 0.1,
                                                  color: dashboard_bg,
                                                  child: ListView.separated(
                                                      separatorBuilder: (context, i) => Divider(
                                                            color: Colors.grey,
                                                            height: 0.1,
                                                          ),
                                                      scrollDirection: Axis.horizontal,
                                                      itemCount: items_arraymenulist[index][j].sizeList.length,
                                                      itemBuilder: (context, i) {
                                                        default_sizeitem = items_arraymenulist[index][j].sizeList[0].sizeName.toString();
                                                        return InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              totalsizelist_selectpos[index][j] = i;
                                                              default_sizeitem = items_arraymenulist[index][j].sizeList[i].sizeName.toString();
                                                              String _itemname = "";
                                                              if (totalsizelist_selectpos[index][j] == -1) {
                                                                //print("bcbccggcgc"+"--"+items_arraymenulist[index][j].itemName);
                                                                _itemname = items_arraymenulist[index][j].itemName;
                                                              } else {
                                                                //print(totalsizelist_selectpos[index][j].toString()+"--"+items_arraymenulist[index][j].itemName+"--"+items_arraymenulist[index][j].sizeList[totalsizelist_selectpos[index][j]].sizeName);
                                                                _itemname = items_arraymenulist[index][j].itemName +
                                                                    " " +
                                                                    items_arraymenulist[index][j].sizeList[totalsizelist_selectpos[index][j]].sizeName;
                                                              }
                                                              addselecteditemwithsize[index].itemId = items_arraymenulist[index][j].itemId;
                                                              addselecteditemwithsize[index].menuId = items_arraymenulist[index][j].menuId;
                                                              addselecteditemwithsize[index].menuGroupId = items_arraymenulist[index][j].menuGroupId;
                                                              addselecteditemwithsize[index].itemName = _itemname;
                                                              print("SIZELISTDATA" + default_sizeitem);
                                                            });
                                                          },
                                                          child: Card(
                                                            elevation: 2,
                                                            color: totalsizelist_selectpos[index][j] == i ? Colors.lightBlueAccent : Colors.white,
                                                            child: Container(
                                                              decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.zero),
                                                              ),
                                                              child: Column(
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Padding(
                                                                      padding: EdgeInsets.only(left: 10, top: 5, right: 10, bottom: 0),
                                                                      child: Text(
                                                                        items_arraymenulist[index][j].sizeList[i].sizeName,
                                                                        style: TextStyle(
                                                                            fontSize: 16,
                                                                            fontFamily: 'Poppins',
                                                                            fontWeight: FontWeight.w500,
                                                                            color: Colors.black),
                                                                      )),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        );
                                                      }))
                                              : SizedBox(
                                                  height: 10,
                                                ),
                                        ],
                                      );
                                    })
                              ],
                            ),
                          );
                        },
                      ),
                    ],
                  ))
            ],
          )))),
          Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                child: Container(
                  // margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  height: 45,
                  color: Colors.lightBlueAccent,
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.only(left: 15, top: 0, right: 10),
                    child: Text(
                      "Done",
                      style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                onTap: () {
                  for (int i = 0; i < addselecteditemwithsize.length; i++) {
                    print(addselecteditemwithsize[i].itemName);
                    if(i==0){
                      addselecteditemwithsize[i].unitPrice =widget.comboprice;
                      addselecteditemwithsize[i].totalPrice =widget.comboprice;
                    }
                    getCartItemModelapi.add(addselecteditemwithsize[i]);
                  }
                  _savecartList(getCartItemModelapi);

                  Navigator.pop(context, "Success");

                  Toast.show("Added to Cart", context,
                      duration: Toast.LENGTH_SHORT,
                      gravity: Toast.BOTTOM);
                },
              ))
        ]));
  }

  _savecartList(cartlist) async {
    final prefs = await SharedPreferences.getInstance();
    final key1 = 'save_to_cartlist';
    final cartlist_value = jsonEncode(cartlist);
    prefs.setString(key1, cartlist_value);
    print('savedCART $cartlist_value');
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(key: key, backgroundColor: Colors.black54, children: <Widget>[
                Center(
                  child: Column(children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Cancelling....",
                      style: TextStyle(color: Colors.lightBlueAccent),
                    )
                  ]),
                )
              ]));
        });
  }
}
