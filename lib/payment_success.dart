import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:waiter/receipt.dart';
import 'package:waiter/utils/all_constans.dart';
import 'package:waiter/utils/sizeconfig.dart';

import 'dashboard.dart';
import 'model/getallmenus.dart';
import 'model/getmenugroups.dart';

class PaymentSuccess extends StatefulWidget {
  final order_id;
  final grand_total;
  final receipt_number;
   PaymentSuccess(this.order_id,this.grand_total,this.receipt_number, {Key key})
      : super(key: key);


  @override
  _PaymentSuccessState createState() => _PaymentSuccessState();
}

class _PaymentSuccessState extends State<PaymentSuccess> {
  List<MenuList> _get_all_menus;
  List<MenugroupsList> _get_menu_groups;
  bool _loading = true;
  int cart_count = 0;
  double total_qty = 0.0;

  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  int selected_pos = 0;
  String userid = "";
  String res_id = "";
  String menu_id = "";
  TextEditingController customtip_Controller = TextEditingController();
  bool customtip_visible = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Container(color:Colors.white,child: Container(margin:EdgeInsets.fromLTRB(15,40,15,15),color:dashboard_bg,child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 20, bottom: 0),
                        child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text: "Order No\n",
                                  style: TextStyle(
                                      color: login_passcode_text,
                                      fontSize: 16,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: "#"+widget.receipt_number,
                                  style: new TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600))
                            ])),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding:
                        EdgeInsets.only(left: 0, right: 15, bottom: 0),
                        child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text: "Amount\n",
                                  style: TextStyle(
                                      color: login_passcode_text,
                                      fontSize: 16,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: new String.fromCharCodes(
                                      new Runes('\u0024')) +
                                      widget.grand_total.toString(),
                                  style: new TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600))
                            ])),
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: 50),
              Expanded (child:(Container(child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                            padding: EdgeInsets.only(left: 0),
                            child: Image.asset(
                              'images/payment_success.png',
                              height: 180,
                              width: 180,
                            ))
                      ],
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
                      child: RichText(
                          text: TextSpan(
                              text: "\t  Payment \n Successful",
                              style: new TextStyle(
                                  fontSize: 28,
                                  color: login_passcode_bg1,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600)))),
                  Container(
                    margin: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: EdgeInsets.only(left: 15, bottom: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                      text: "Total Amount :",
                                      style: new TextStyle(
                                          fontSize: 18,
                                          color: login_passcode_text,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w500))
                                ],
                              )),
                          RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                    text: new String.fromCharCodes(
                                        new Runes('\u0024')) +
                                        widget.grand_total.toString(),
                                    style: new TextStyle(
                                        fontSize: 24,
                                        color: login_passcode_text,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w700))
                              ]))
                        ],
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(75, 75, 75, 0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: login_passcode_bg1,
                          borderRadius: BorderRadius.circular(0)),
                      child: InkWell(
                          child: FlatButton(
                              child: Text("RECEIPT",
                                  style: TextStyle(
                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white)),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Receipt(widget.receipt_number)));
                              }))),
                  Container(
                      margin: EdgeInsets.fromLTRB(75, 20, 75, 0),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: dashboard_quick_order,
                          borderRadius: BorderRadius.circular(0)),
                      child: InkWell(
                          child: FlatButton(
                              child: Text("GO TO DASHBOARD",
                                  style: TextStyle(
                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white)),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => HomePage()));
                                /*Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => AddFood()));*/})))],
              ),) ),
              ),


            ],
          ),))
        ),);
  }
}

class CancelDialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Cancelling....",
                          style: TextStyle(color: Colors.lightBlueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
