import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:waiter/model/addbogoitemstatus.dart';
import 'package:waiter/model/addcomboitemstatus.dart';
import 'package:waiter/model/cartmodelitemsapi.dart';
import 'package:waiter/utils/all_constans.dart';

class UserRepository {


  static Future save_userid(String userid,String firstname,String lastname,String isLoginWaiter) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyUserid, userid);
    prefs.setString(keyFirstName, firstname);
    prefs.setString(keyLastName, lastname);
    prefs.setString(keyIsLoginWaiter, isLoginWaiter);
  }
  Future<List<String>> getuserdetails()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final userid = prefs.getString(keyUserid) ?? "";
    final firstname = prefs.getString(keyFirstName) ?? "";
    final lastname = prefs.getString(keyLastName) ?? "";
    final is_loginwaiter = prefs.getString(keyIsLoginWaiter) ?? "";

    List<String> user_data = [userid,firstname,lastname,is_loginwaiter];

    return user_data;
  }


  static Future save_restaurantid(String restaurantid,String isLogin) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyRestaurantId_With_Code, restaurantid);
    prefs.setString(keyIsLogin, isLogin);
  }
  Future<List<String>> getRestaurant_id()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final restaurant_id = prefs.getString(keyRestaurantId) ?? "";
    final is_login = prefs.getString(keyIsLogin) ?? "";

    List<String> generaterestautrant_data = [restaurant_id,is_login];
    return generaterestautrant_data;
  }


  static Future save_restaurant_code(String restaurantcode) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyRestaurantCode, restaurantcode);
  }
  Future<List<String>> getRestaurantCode()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final restaurant_code = prefs.getString(keyRestaurantCode) ?? "";
    List<String> generaterestautran_code = [restaurant_code];
    return generaterestautran_code;
  }


  static Future save_servicearea_id(String serviceareaid,String revenuecentreid) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyServiceAreaId, serviceareaid);
    prefs.setString(keyRevenuecentreId, revenuecentreid);
  }
  Future<List<String>> getServiceAreaandCentreId()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final service_area_id = prefs.getString(keyServiceAreaId) ?? "";
    final revenue_centre_id = prefs.getString(keyRevenuecentreId) ?? "";

    List<String> generaterevenuecentre_data = [service_area_id,revenue_centre_id];
    return generaterevenuecentre_data;
  }


  static Future save_OrderId(String orderid) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyOrderId, orderid);
  }
  Future<List<String>> getOrderId()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final order_id = prefs.getString(keyOrderId) ?? "";

    List<String> getorderid_data = [order_id];
    return getorderid_data;
  }


  static Future save_dineinoption_id(String dineinoptionid) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyDineInOptionId, dineinoptionid);
  }
  Future<List<String>> getDineInOptionId()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final dine_in_option_id = prefs.getString(keyDineInOptionId) ?? "";

    List<String> generatedineinoption_data = [dine_in_option_id];
    return generatedineinoption_data;
  }


  static Future save_otp(String restaurant_id) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyRestaurantId, restaurant_id);
  }
  Future<List<String>> getGenerateOtpDetails()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final restaurant_id = prefs.getString(keyRestaurantId) ?? "";

    List<String> generateotp_data = [restaurant_id];
    return generateotp_data;
  }
  static Future save_TablenumbernGuests(String table_number,String no_of_guests) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyTableNumber, table_number);
    prefs.setString(keyNoOfGuests, no_of_guests);
  }
  Future<List<String>> getGenerateTablenumbernGuests()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final table_numer = prefs.getString(keyTableNumber) ?? "";
    final no_of_guests = prefs.getString(keyNoOfGuests) ?? "";

    List<String> generatetable_service_data = [table_numer,no_of_guests];
    return generatetable_service_data;
  }
  savecombo_with_types(comboidwithtypes) async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'save_comboidwithtypes';
    final combolist_value = jsonEncode(comboidwithtypes);
    prefs.setString(key, combolist_value);
    print('save_comboidwithtypes $combolist_value');
  }
  savebogo_with_types(bogoidwithtypes) async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'save_bogoidwithtypes';
    final bogolist_value = jsonEncode(bogoidwithtypes);
    prefs.setString(key, bogolist_value);
    print('save_bogoidwithtypes $bogolist_value');
  }
  Future<List<addcomboitemstatus>> getcombo_with_types()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final dine_in_option_id = prefs.getString("save_comboidwithtypes") ?? "[]";

    Iterable list = json.decode(dine_in_option_id);
    return list.map((model) => addcomboitemstatus.fromJson(model)).toList();
  }
  Future<List<addbogoitemstatus>> getbogo_with_types()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final dine_in_option_id = prefs.getString("save_bogoidwithtypes") ?? "[]";

    Iterable list = json.decode(dine_in_option_id);
    return list.map((model) => addbogoitemstatus.fromJson(model)).toList();
  }

  Future<List<MenuCartItemapi>> saveselectedbuyitems(buyitems_list) async{
    final prefs = await SharedPreferences.getInstance();
    final key1 = 'save_selectedbuyitems';
    final cartlist_value = jsonEncode(buyitems_list);
    prefs.setString(key1, cartlist_value);
    print('save_bogoidwithtypes $cartlist_value');
  }
  Future<List<MenuCartItemapi>> getselectedbuyitems()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final dine_in_option_id = prefs.getString("save_selectedbuyitems") ?? "[]";

    Iterable list = json.decode(dine_in_option_id);
    return list.map((model) => MenuCartItemapi.fromJson(model)).toList();
  }



  static Future Clearall() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  static Future removesaved_tablenumberguests() async {
    print("CLEARCART============");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('save_to_cartlist');
  }

}