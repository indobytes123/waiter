

import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/getmenuitems.dart';

class gettableinformationresponse {
  int responseStatus;
  String result;
  List<TableInfo> orderInfo;

  gettableinformationresponse({this.responseStatus, this.result, this.orderInfo});

  gettableinformationresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['orderInfo'] != null) {
      orderInfo = new List<TableInfo>();
      json['orderInfo'].forEach((v) {
        orderInfo.add(new TableInfo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.orderInfo != null) {
      data['orderInfo'] = this.orderInfo.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TableInfo {
  int checkNumber;
  String createdBy;
  String createdOn;
  double creditsUsed;
  CustomerDetails customerDetails;
  String dineInBehaviour;
  String dineInOptionId;
  String dineInOptionName;
  double discountAmount;
  bool haveCustomer;
  List<ItemsList> itemsList;
  int noOfGuest;
  String orderId;
  String orderUniqueId;
  int orderNumber;
  String revenueCenterId;
  String serviceAreaId;
  String subTotal;
  int tableNumber;
  int tableStatus;
  String taxAmount;
  String tipAmount;
  String totalAmount;
  int paymentStatus;
  String receiptNumber;

  TableInfo(
      this.checkNumber,
        this.createdBy,
        this.createdOn,
      this.creditsUsed,
        this.customerDetails,
        this.dineInBehaviour,
        this.dineInOptionId,
        this.dineInOptionName,
        this.discountAmount,
      this.haveCustomer,
        this.itemsList,
        this.noOfGuest,
        this.orderId,
        this.orderUniqueId,
        this.orderNumber,
      this.revenueCenterId,
      this.serviceAreaId,
        this.subTotal,
        this.tableNumber,
        this.tableStatus,
        this.taxAmount,
        this.tipAmount,
        this.totalAmount,
      this.paymentStatus, this.receiptNumber,);

  TableInfo.fromJson(Map<String, dynamic> json) {
    checkNumber = json['checkNumber'] as int;
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    creditsUsed = json['creditsUsed'];
    customerDetails = json['customerDetails'] != null
        ? new CustomerDetails.fromJson(json['customerDetails'])
        : null;
    dineInBehaviour = json['dineInBehaviour'];
    dineInOptionId = json['dineInOptionId'];
    dineInOptionName = json['dineInOptionName'];
    discountAmount = json['discountAmount'];
    haveCustomer = json['haveCustomer'];
    if (json['itemsList'] != null) {
      itemsList = new List<ItemsList>();
      json['itemsList'].forEach((v) {
        itemsList.add(new ItemsList.fromJson(v));
      });
    }
    noOfGuest = json['noOfGuest'];
    orderId = json['orderId'];
    orderUniqueId = json['orderUniqueId'];
    orderNumber = json['orderNumber'];
    revenueCenterId = json['revenueCenterId'];
    serviceAreaId = json['serviceAreaId'];
    subTotal = json['subTotal'] as String;
    tableNumber = json['tableNumber'];
    tableStatus = json['tableStatus'];
    taxAmount = json['taxAmount'] as String;
    tipAmount = json['tipAmount'] as String;
    totalAmount = json['totalAmount'] as String;
    paymentStatus = json['paymentStatus'];
    receiptNumber = json['receiptNumber'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['checkNumber'] = this.checkNumber;
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['creditsUsed'] = this.creditsUsed;
    if (this.customerDetails != null) {
      data['customerDetails'] = this.customerDetails.toJson();
    }
    data['dineInBehaviour'] = this.dineInBehaviour;
    data['dineInOptionId'] = this.dineInOptionId;
    data['dineInOptionName'] = this.dineInOptionName;
    data['discountAmount'] = this.discountAmount;
    data['haveCustomer'] = this.haveCustomer;
    if (this.itemsList != null) {
      data['itemsList'] = this.itemsList.map((v) => v.toJson()).toList();
    }
    data['noOfGuest'] = this.noOfGuest;
    data['orderId'] = this.orderId;
    data['orderUniqueId'] = this.orderUniqueId;
    data['orderNumber'] = this.orderNumber;
    data['revenueCenterId'] = this.revenueCenterId;
    data['serviceAreaId'] = this.serviceAreaId;
    data['subTotal'] = this.subTotal;
    data['tableNumber'] = this.tableNumber;
    data['tableStatus'] = this.tableStatus;
    data['taxAmount'] = this.taxAmount;
    data['receiptNumber'] = this.receiptNumber;
    data['tipAmount'] = this.tipAmount;
    data['totalAmount'] = this.totalAmount;
    data['paymentStatus'] = this.paymentStatus;
    return data;
  }
}
class CustomerDetails {
  String customerId;
  String email;
  String firstName;
  String lastName;
  String phoneNumber;

  CustomerDetails(
      {this.customerId,
        this.email,
        this.firstName,
        this.lastName,
        this.phoneNumber});

  CustomerDetails.fromJson(Map<String, dynamic> json) {
    customerId = json['customerId'];
    email = json['email'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    phoneNumber = json['phoneNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customerId'] = this.customerId;
    data['email'] = this.email;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['phoneNumber'] = this.phoneNumber;
    return data;
  }
}
class ItemsList {
  String discountAmount;
  String discountName;
  String discountType;
  String discountValue;
  bool isBogo;
  bool isCombo;
  String itemId;
  String itemName;
  int itemStatus;
  String itemType;
  List<ModifiersList> modifiersList;
  List<ItemTaxRates> itemTaxesList;
  int quantity;
  List<SpecialRequestList> specialRequestList;
  String totalPrice;
  String unitPrice;

  ItemsList(
      this.discountAmount,
        this.discountName,
        this.discountType,
        this.discountValue,
        this.isBogo,
        this.isCombo,
        this.itemId,
        this.itemName,
        this.itemStatus,
        this.itemType,
        this.modifiersList,
        this.itemTaxesList,
        this.quantity,
        this.specialRequestList,
        this.totalPrice,
        this.unitPrice);

  ItemsList.fromJson(Map<String, dynamic> json) {
    discountAmount = json['discountAmount'] as String;
    discountName = json['discountName'];
    discountType = json['discountType'];
    discountValue = json['discountValue'] as String;
    isBogo = json['isBogo'];
    isCombo = json['isCombo'];
    itemId = json['itemId'];
    itemName = json['itemName'];
    itemStatus = json['itemStatus'];
    itemType = json['itemType'];
    if (json['modifiersList'] != null) {
      modifiersList = new List<ModifiersList>();
      json['modifiersList'].forEach((v) {
        modifiersList.add(new ModifiersList.fromJson(v));
      });
    }
    if (json['itemTaxesList'] != null) {
      itemTaxesList = new List<ItemTaxRates>();
      json['itemTaxesList'].forEach((v) {
        itemTaxesList.add(new ItemTaxRates.fromJson(v));
      });
    }
    quantity = json['quantity'];
    if (json['specialRequestList'] != null) {
      specialRequestList = new List<SpecialRequestList>();
      json['specialRequestList'].forEach((v) {
        specialRequestList.add(new SpecialRequestList.fromJson(v));
      });
    }
    totalPrice = json['totalPrice'] as String;
    unitPrice = json['unitPrice'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['discountAmount'] = this.discountAmount;
    data['discountName'] = this.discountName;
    data['discountType'] = this.discountType;
    data['discountValue'] = this.discountValue;
    data['isBogo'] = this.isBogo;
    data['isCombo'] = this.isCombo;
    data['itemId'] = this.itemId;
    data['itemName'] = this.itemName;
    data['itemStatus'] = this.itemStatus;
    data['itemType'] = this.itemType;
    if (this.modifiersList != null) {
      data['modifiersList'] =
          this.modifiersList.map((v) => v.toJson()).toList();
    }
    if (this.itemTaxesList != null) {
      data['itemTaxesList'] =
          this.itemTaxesList.map((v) => v.toJson()).toList();
    }
    data['quantity'] = this.quantity;
    if (this.specialRequestList != null) {
      data['specialRequestList'] =
          this.specialRequestList.map((v) => v.toJson()).toList();
    }
    data['totalPrice'] = this.totalPrice;
    data['unitPrice'] = this.unitPrice;
    return data;
  }
}

class ModifiersList {
  String modifierId;
  String modifierName;
  int modifierQty;
  double modifierTotalPrice;
  double modifierUnitPrice;

  ModifiersList(
      {this.modifierId,
        this.modifierName,
        this.modifierQty,
        this.modifierTotalPrice,
        this.modifierUnitPrice});

  ModifiersList.fromJson(Map<String, dynamic> json) {
    modifierId = json['modifierId'];
    modifierName = json['modifierName'];
    modifierQty = json['modifierQty'];
    modifierTotalPrice = json['modifierTotalPrice'] as double;
    modifierUnitPrice = json['modifierUnitPrice']as double;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['modifierId'] = this.modifierId;
    data['modifierName'] = this.modifierName;
    data['modifierQty'] = this.modifierQty;
    data['modifierTotalPrice'] = this.modifierTotalPrice;
    data['modifierUnitPrice'] = this.modifierUnitPrice;
    return data;
  }
}

class SpecialRequestList {
  String name;
  int requestPrice;

  SpecialRequestList({this.name, this.requestPrice});

  SpecialRequestList.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    requestPrice = json['requestPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['requestPrice'] = this.requestPrice;
    return data;
  }
}



class ItemTaxRates {

  bool enableTakeOutRate;
  String importId;
  int orderValue;
  int roundingOptions;
  int status;
  double tax;
  String taxName;
  double taxRate;
  List<TaxTable> taxTable;
  int taxTypeId;
  String taxType;
  String taxid;
  int uniqueNumber;

  ItemTaxRates({this.enableTakeOutRate, this.importId, this.orderValue, this.roundingOptions, this.status, this.tax,this.taxName, this.taxRate, this.taxTable,this.taxTypeId, this.taxType, this.taxid, this.uniqueNumber});

  ItemTaxRates.fromJson(Map<String, dynamic> json) {
    /* default = json['default'];*/
    enableTakeOutRate = json['enableTakeOutRate'];
    importId = json['importId'];
    orderValue = json['orderValue'];
    roundingOptions = json['roundingOptions'];
    status = json['status'];
    tax = json['tax'];
    taxName = json['taxName'];
    taxRate = json['taxRate'];
    if (json['taxTable'] != null) {
      taxTable = new List<TaxTable>();
      json['taxTable'].forEach((v) { taxTable.add(new TaxTable.fromJson(v)); });
    }
    taxTypeId = json['taxTypeId'];
    taxType = json['taxType'] ;
    taxid = json['taxid'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    /*data['default'] = this.default;*/
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['tax'] = this.tax;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    if (this.taxTable != null) {
      data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    }
    data['taxTypeId'] = this.taxTypeId;
    data['taxType'] = this.taxType;
    data['taxid'] = this.taxid;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

class TaxTable {
  String from;
  double priceDifference;
  bool repeat;
  String taxApplied;
  String to;

  TaxTable({ this.from, this.priceDifference, this.repeat, this.taxApplied,this.to});

  TaxTable.fromJson(Map<String, dynamic> json) {
    from = json['from'];
    priceDifference = json['priceDifference'] as double;
    repeat = json['repeat'];
    taxApplied = json['taxApplied'];
    to = json['to'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['from'] = this.from;
    data['priceDifference'] = this.priceDifference;
    data['repeat'] = this.repeat;
    data['taxApplied'] = this.taxApplied;
    data['to'] = this.to;
    return data;
  }
}