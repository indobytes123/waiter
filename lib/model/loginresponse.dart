class loginresponse {
  int responseStatus;
  int due;
  int paid;
  String result;
  String receiptNumber;
  UserDetails userDetails;

  loginresponse({this.responseStatus, this.result,this.receiptNumber, this.userDetails});

  loginresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    receiptNumber = json['receiptNumber'];
    userDetails = json['userDetails'] != null
        ? new UserDetails.fromJson(json['userDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    data['receiptNumber'] = this.receiptNumber;
    if (this.userDetails != null) {
      data['userDetails'] = this.userDetails.toJson();
    }
    return data;
  }
}

class UserDetails {
  String empId;
  String firstName;
  String id;
  String lastName;
  PermissionList permissionList;
  String profilePic;
  String restaurantId;

  UserDetails(
      {this.empId,
        this.firstName,
        this.id,
        this.lastName,
        this.permissionList,
        this.profilePic,
        this.restaurantId});

  UserDetails.fromJson(Map<String, dynamic> json) {
    empId = json['empId'];
    firstName = json['firstName'];
    id = json['id'];
    lastName = json['lastName'];
    permissionList = json['permissionList'] != null
        ? new PermissionList.fromJson(json['permissionList'])
        : null;
    profilePic = json['profilePic'];
    restaurantId = json['restaurantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['empId'] = this.empId;
    data['firstName'] = this.firstName;
    data['id'] = this.id;
    data['lastName'] = this.lastName;
    if (this.permissionList != null) {
      data['permissionList'] = this.permissionList.toJson();
    }
    data['profilePic'] = this.profilePic;
    data['restaurantId'] = this.restaurantId;
    return data;
  }
}

class PermissionList {
  AccountAdminAccess accountAdminAccess;
  DeliveryAccess deliveryAccess;
  DeviceSetupAccess deviceSetupAccess;
  ManagerAccess managerAccess;
  PosAccess posAccess;
  QuickEditAccess quickEditAccess;
  RestaurantAdminAccess restaurantAdminAccess;
  WebSetupAccess webSetupAccess;

  PermissionList(
      {this.accountAdminAccess,
        this.deliveryAccess,
        this.deviceSetupAccess,
        this.managerAccess,
        this.posAccess,
        this.quickEditAccess,
        this.restaurantAdminAccess,
        this.webSetupAccess});

  PermissionList.fromJson(Map<String, dynamic> json) {
    accountAdminAccess = json['accountAdminAccess'] != null
        ? new AccountAdminAccess.fromJson(json['accountAdminAccess'])
        : null;
    deliveryAccess = json['deliveryAccess'] != null
        ? new DeliveryAccess.fromJson(json['deliveryAccess'])
        : null;
    deviceSetupAccess = json['deviceSetupAccess'] != null
        ? new DeviceSetupAccess.fromJson(json['deviceSetupAccess'])
        : null;
    managerAccess = json['managerAccess'] != null
        ? new ManagerAccess.fromJson(json['managerAccess'])
        : null;
    posAccess = json['posAccess'] != null
        ? new PosAccess.fromJson(json['posAccess'])
        : null;
    quickEditAccess = json['quickEditAccess'] != null
        ? new QuickEditAccess.fromJson(json['quickEditAccess'])
        : null;
    restaurantAdminAccess = json['restaurantAdminAccess'] != null
        ? new RestaurantAdminAccess.fromJson(json['restaurantAdminAccess'])
        : null;
    webSetupAccess = json['webSetupAccess'] != null
        ? new WebSetupAccess.fromJson(json['webSetupAccess'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.accountAdminAccess != null) {
      data['accountAdminAccess'] = this.accountAdminAccess.toJson();
    }
    if (this.deliveryAccess != null) {
      data['deliveryAccess'] = this.deliveryAccess.toJson();
    }
    if (this.deviceSetupAccess != null) {
      data['deviceSetupAccess'] = this.deviceSetupAccess.toJson();
    }
    if (this.managerAccess != null) {
      data['managerAccess'] = this.managerAccess.toJson();
    }
    if (this.posAccess != null) {
      data['posAccess'] = this.posAccess.toJson();
    }
    if (this.quickEditAccess != null) {
      data['quickEditAccess'] = this.quickEditAccess.toJson();
    }
    if (this.restaurantAdminAccess != null) {
      data['restaurantAdminAccess'] = this.restaurantAdminAccess.toJson();
    }
    if (this.webSetupAccess != null) {
      data['webSetupAccess'] = this.webSetupAccess.toJson();
    }
    return data;
  }
}

class AccountAdminAccess {
  bool dataExportConfig;
  bool financialAccounts;
  bool manageIntegrations;
  bool userPermissions;

  AccountAdminAccess(
      {this.dataExportConfig,
        this.financialAccounts,
        this.manageIntegrations,
        this.userPermissions});

  AccountAdminAccess.fromJson(Map<String, dynamic> json) {
    dataExportConfig = json['dataExportConfig'];
    financialAccounts = json['financialAccounts'];
    manageIntegrations = json['manageIntegrations'];
    userPermissions = json['userPermissions'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dataExportConfig'] = this.dataExportConfig;
    data['financialAccounts'] = this.financialAccounts;
    data['manageIntegrations'] = this.manageIntegrations;
    data['userPermissions'] = this.userPermissions;
    return data;
  }
}

class DeliveryAccess {
  bool cancelDispatch;
  bool completeDelivery;
  bool deliveryMode;
  bool dispatchDriver;
  bool updateAllDeliveryOrders;
  bool updateDriver;

  DeliveryAccess(
      {this.cancelDispatch,
        this.completeDelivery,
        this.deliveryMode,
        this.dispatchDriver,
        this.updateAllDeliveryOrders,
        this.updateDriver});

  DeliveryAccess.fromJson(Map<String, dynamic> json) {
    cancelDispatch = json['cancelDispatch'];
    completeDelivery = json['completeDelivery'];
    deliveryMode = json['deliveryMode'];
    dispatchDriver = json['dispatchDriver'];
    updateAllDeliveryOrders = json['updateAllDeliveryOrders'];
    updateDriver = json['updateDriver'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cancelDispatch'] = this.cancelDispatch;
    data['completeDelivery'] = this.completeDelivery;
    data['deliveryMode'] = this.deliveryMode;
    data['dispatchDriver'] = this.dispatchDriver;
    data['updateAllDeliveryOrders'] = this.updateAllDeliveryOrders;
    data['updateDriver'] = this.updateDriver;
    return data;
  }
}

class DeviceSetupAccess {
  bool advancedTerminalSetup;
  bool kdsAndOrderScreenSetup;
  bool terminalSetup;

  DeviceSetupAccess(
      {this.advancedTerminalSetup,
        this.kdsAndOrderScreenSetup,
        this.terminalSetup});

  DeviceSetupAccess.fromJson(Map<String, dynamic> json) {
    advancedTerminalSetup = json['advancedTerminalSetup'];
    kdsAndOrderScreenSetup = json['kdsAndOrderScreenSetup'];
    terminalSetup = json['terminalSetup'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['advancedTerminalSetup'] = this.advancedTerminalSetup;
    data['kdsAndOrderScreenSetup'] = this.kdsAndOrderScreenSetup;
    data['terminalSetup'] = this.terminalSetup;
    return data;
  }
}

class ManagerAccess {
  bool adjustCashDrawerStartBalance;
  bool bulkClosePaidChecks;
  bool bulkTransferCheck;
  bool bulkVoidOpenChecks;
  bool cashDrawerLockdown;
  bool cashDrawersBlind;
  bool cashDrawersFull;
  bool closeOutDay;
  bool discounts;
  bool editSentItems;
  bool editTimeEntries;
  bool endBreaksEarly;
  bool findChecks;
  bool giftCardAdjustment;
  bool largeCashOverOrUnder;
  bool logBook;
  bool negativeDeclaredTips;
  bool openItems;
  bool otherPaymentTypes;
  bool payout;
  bool registerSwipeCards;
  bool sendNotifications;
  bool shiftReview;
  bool taxExempt;
  bool throttleOnlineOrders;
  bool transferOrRewardsAdjustment;
  bool unlinkedRefunds;
  bool voidItemOrOrders;
  bool voidOrRefundPayments;

  ManagerAccess(
      {this.adjustCashDrawerStartBalance,
        this.bulkClosePaidChecks,
        this.bulkTransferCheck,
        this.bulkVoidOpenChecks,
        this.cashDrawerLockdown,
        this.cashDrawersBlind,
        this.cashDrawersFull,
        this.closeOutDay,
        this.discounts,
        this.editSentItems,
        this.editTimeEntries,
        this.endBreaksEarly,
        this.findChecks,
        this.giftCardAdjustment,
        this.largeCashOverOrUnder,
        this.logBook,
        this.negativeDeclaredTips,
        this.openItems,
        this.otherPaymentTypes,
        this.payout,
        this.registerSwipeCards,
        this.sendNotifications,
        this.shiftReview,
        this.taxExempt,
        this.throttleOnlineOrders,
        this.transferOrRewardsAdjustment,
        this.unlinkedRefunds,
        this.voidItemOrOrders,
        this.voidOrRefundPayments});

  ManagerAccess.fromJson(Map<String, dynamic> json) {
    adjustCashDrawerStartBalance = json['adjustCashDrawerStartBalance'];
    bulkClosePaidChecks = json['bulkClosePaidChecks'];
    bulkTransferCheck = json['bulkTransferCheck'];
    bulkVoidOpenChecks = json['bulkVoidOpenChecks'];
    cashDrawerLockdown = json['cashDrawerLockdown'];
    cashDrawersBlind = json['cashDrawersBlind'];
    cashDrawersFull = json['cashDrawersFull'];
    closeOutDay = json['closeOutDay'];
    discounts = json['discounts'];
    editSentItems = json['editSentItems'];
    editTimeEntries = json['editTimeEntries'];
    endBreaksEarly = json['endBreaksEarly'];
    findChecks = json['findChecks'];
    giftCardAdjustment = json['giftCardAdjustment'];
    largeCashOverOrUnder = json['largeCashOverOrUnder'];
    logBook = json['logBook'];
    negativeDeclaredTips = json['negativeDeclaredTips'];
    openItems = json['openItems'];
    otherPaymentTypes = json['otherPaymentTypes'];
    payout = json['payout'];
    registerSwipeCards = json['registerSwipeCards'];
    sendNotifications = json['sendNotifications'];
    shiftReview = json['shiftReview'];
    taxExempt = json['taxExempt'];
    throttleOnlineOrders = json['throttleOnlineOrders'];
    transferOrRewardsAdjustment = json['transferOrRewardsAdjustment'];
    unlinkedRefunds = json['unlinkedRefunds'];
    voidItemOrOrders = json['voidItemOrOrders'];
    voidOrRefundPayments = json['voidOrRefundPayments'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adjustCashDrawerStartBalance'] = this.adjustCashDrawerStartBalance;
    data['bulkClosePaidChecks'] = this.bulkClosePaidChecks;
    data['bulkTransferCheck'] = this.bulkTransferCheck;
    data['bulkVoidOpenChecks'] = this.bulkVoidOpenChecks;
    data['cashDrawerLockdown'] = this.cashDrawerLockdown;
    data['cashDrawersBlind'] = this.cashDrawersBlind;
    data['cashDrawersFull'] = this.cashDrawersFull;
    data['closeOutDay'] = this.closeOutDay;
    data['discounts'] = this.discounts;
    data['editSentItems'] = this.editSentItems;
    data['editTimeEntries'] = this.editTimeEntries;
    data['endBreaksEarly'] = this.endBreaksEarly;
    data['findChecks'] = this.findChecks;
    data['giftCardAdjustment'] = this.giftCardAdjustment;
    data['largeCashOverOrUnder'] = this.largeCashOverOrUnder;
    data['logBook'] = this.logBook;
    data['negativeDeclaredTips'] = this.negativeDeclaredTips;
    data['openItems'] = this.openItems;
    data['otherPaymentTypes'] = this.otherPaymentTypes;
    data['payout'] = this.payout;
    data['registerSwipeCards'] = this.registerSwipeCards;
    data['sendNotifications'] = this.sendNotifications;
    data['shiftReview'] = this.shiftReview;
    data['taxExempt'] = this.taxExempt;
    data['throttleOnlineOrders'] = this.throttleOnlineOrders;
    data['transferOrRewardsAdjustment'] = this.transferOrRewardsAdjustment;
    data['unlinkedRefunds'] = this.unlinkedRefunds;
    data['voidItemOrOrders'] = this.voidItemOrOrders;
    data['voidOrRefundPayments'] = this.voidOrRefundPayments;
    return data;
  }
}

class PosAccess {
  bool addOrUpdateServiceCharges;
  bool applyCashPayments;
  bool cashDrawerAccess;
  bool changeServer;
  bool changeTable;
  bool editOtherEmployeesOrders;
  bool keyInCreditCards;
  bool kitchenDisplayScreenMode;
  bool myReports;
  bool noSale;
  bool offlineOrBackgroundCreditCardProcessing;
  bool pandingOrdersMode;
  bool paymentTerminalMode;
  bool quickOrderMode;
  bool shiftReviewSalesData;
  bool tableServiceMode;
  bool viewOtherEmployeesOrders;

  PosAccess(
      {this.addOrUpdateServiceCharges,
        this.applyCashPayments,
        this.cashDrawerAccess,
        this.changeServer,
        this.changeTable,
        this.editOtherEmployeesOrders,
        this.keyInCreditCards,
        this.kitchenDisplayScreenMode,
        this.myReports,
        this.noSale,
        this.offlineOrBackgroundCreditCardProcessing,
        this.pandingOrdersMode,
        this.paymentTerminalMode,
        this.quickOrderMode,
        this.shiftReviewSalesData,
        this.tableServiceMode,
        this.viewOtherEmployeesOrders});

  PosAccess.fromJson(Map<String, dynamic> json) {
    addOrUpdateServiceCharges = json['addOrUpdateServiceCharges'];
    applyCashPayments = json['applyCashPayments'];
    cashDrawerAccess = json['cashDrawerAccess'];
    changeServer = json['changeServer'];
    changeTable = json['changeTable'];
    editOtherEmployeesOrders = json['editOtherEmployeesOrders'];
    keyInCreditCards = json['keyInCreditCards'];
    kitchenDisplayScreenMode = json['kitchenDisplayScreenMode'];
    myReports = json['myReports'];
    noSale = json['noSale'];
    offlineOrBackgroundCreditCardProcessing =
    json['offlineOrBackgroundCreditCardProcessing'];
    pandingOrdersMode = json['pandingOrdersMode'];
    paymentTerminalMode = json['paymentTerminalMode'];
    quickOrderMode = json['quickOrderMode'];
    shiftReviewSalesData = json['shiftReviewSalesData'];
    tableServiceMode = json['tableServiceMode'];
    viewOtherEmployeesOrders = json['viewOtherEmployeesOrders'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addOrUpdateServiceCharges'] = this.addOrUpdateServiceCharges;
    data['applyCashPayments'] = this.applyCashPayments;
    data['cashDrawerAccess'] = this.cashDrawerAccess;
    data['changeServer'] = this.changeServer;
    data['changeTable'] = this.changeTable;
    data['editOtherEmployeesOrders'] = this.editOtherEmployeesOrders;
    data['keyInCreditCards'] = this.keyInCreditCards;
    data['kitchenDisplayScreenMode'] = this.kitchenDisplayScreenMode;
    data['myReports'] = this.myReports;
    data['noSale'] = this.noSale;
    data['offlineOrBackgroundCreditCardProcessing'] =
        this.offlineOrBackgroundCreditCardProcessing;
    data['pandingOrdersMode'] = this.pandingOrdersMode;
    data['paymentTerminalMode'] = this.paymentTerminalMode;
    data['quickOrderMode'] = this.quickOrderMode;
    data['shiftReviewSalesData'] = this.shiftReviewSalesData;
    data['tableServiceMode'] = this.tableServiceMode;
    data['viewOtherEmployeesOrders'] = this.viewOtherEmployeesOrders;
    return data;
  }
}

class QuickEditAccess {
  bool addExistingItemsOrMods;
  bool addNewItemsMods;
  bool buttonColor;
  bool fullQuickEdit;
  bool inventoryAndQuantity;
  bool name;
  bool posName;
  bool price;
  bool rearrangingItemsMods;
  bool removeItemsOrMods;
  bool sku;

  QuickEditAccess(
      {this.addExistingItemsOrMods,
        this.addNewItemsMods,
        this.buttonColor,
        this.fullQuickEdit,
        this.inventoryAndQuantity,
        this.name,
        this.posName,
        this.price,
        this.rearrangingItemsMods,
        this.removeItemsOrMods,
        this.sku});

  QuickEditAccess.fromJson(Map<String, dynamic> json) {
    addExistingItemsOrMods = json['addExistingItemsOrMods'];
    addNewItemsMods = json['addNewItemsMods'];
    buttonColor = json['buttonColor'];
    fullQuickEdit = json['fullQuickEdit'];
    inventoryAndQuantity = json['inventoryAndQuantity'];
    name = json['name'];
    posName = json['posName'];
    price = json['price'];
    rearrangingItemsMods = json['rearrangingItemsMods'];
    removeItemsOrMods = json['removeItemsOrMods'];
    sku = json['sku'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addExistingItemsOrMods'] = this.addExistingItemsOrMods;
    data['addNewItemsMods'] = this.addNewItemsMods;
    data['buttonColor'] = this.buttonColor;
    data['fullQuickEdit'] = this.fullQuickEdit;
    data['inventoryAndQuantity'] = this.inventoryAndQuantity;
    data['name'] = this.name;
    data['posName'] = this.posName;
    data['price'] = this.price;
    data['rearrangingItemsMods'] = this.rearrangingItemsMods;
    data['removeItemsOrMods'] = this.removeItemsOrMods;
    data['sku'] = this.sku;
    return data;
  }
}

class RestaurantAdminAccess {
  bool customersCreditsAndReports;
  bool editFullMenu;
  bool editHistoricalData;
  bool employeeInfo;
  bool employeeJobsAndWages;
  bool giftOrRewardsCardReport;
  bool houseAccounts;
  bool laborReports;
  bool localMenuEdit;
  bool marketingInfo;
  bool menuReports;
  bool salesReports;
  bool tables;

  RestaurantAdminAccess(
      {this.customersCreditsAndReports,
        this.editFullMenu,
        this.editHistoricalData,
        this.employeeInfo,
        this.employeeJobsAndWages,
        this.giftOrRewardsCardReport,
        this.houseAccounts,
        this.laborReports,
        this.localMenuEdit,
        this.marketingInfo,
        this.menuReports,
        this.salesReports,
        this.tables});

  RestaurantAdminAccess.fromJson(Map<String, dynamic> json) {
    customersCreditsAndReports = json['customersCreditsAndReports'];
    editFullMenu = json['editFullMenu'];
    editHistoricalData = json['editHistoricalData'];
    employeeInfo = json['employeeInfo'];
    employeeJobsAndWages = json['employeeJobsAndWages'];
    giftOrRewardsCardReport = json['giftOrRewardsCardReport'];
    houseAccounts = json['houseAccounts'];
    laborReports = json['laborReports'];
    localMenuEdit = json['localMenuEdit'];
    marketingInfo = json['marketingInfo'];
    menuReports = json['menuReports'];
    salesReports = json['salesReports'];
    tables = json['tables'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customersCreditsAndReports'] = this.customersCreditsAndReports;
    data['editFullMenu'] = this.editFullMenu;
    data['editHistoricalData'] = this.editHistoricalData;
    data['employeeInfo'] = this.employeeInfo;
    data['employeeJobsAndWages'] = this.employeeJobsAndWages;
    data['giftOrRewardsCardReport'] = this.giftOrRewardsCardReport;
    data['houseAccounts'] = this.houseAccounts;
    data['laborReports'] = this.laborReports;
    data['localMenuEdit'] = this.localMenuEdit;
    data['marketingInfo'] = this.marketingInfo;
    data['menuReports'] = this.menuReports;
    data['salesReports'] = this.salesReports;
    data['tables'] = this.tables;
    return data;
  }
}

class WebSetupAccess {
  bool dataExportConfig;
  bool discountsSetup;
  bool financialAccounts;
  bool kitchenOrDiningRoomSetup;
  bool manageInstructions;
  bool paymentsSetup;
  bool publishing;
  bool restaurantGroupsSetup;
  bool restaurantOperationsSetup;
  bool taxRatesSetup;
  bool userPermissions;

  WebSetupAccess(
      {this.dataExportConfig,
        this.discountsSetup,
        this.financialAccounts,
        this.kitchenOrDiningRoomSetup,
        this.manageInstructions,
        this.paymentsSetup,
        this.publishing,
        this.restaurantGroupsSetup,
        this.restaurantOperationsSetup,
        this.taxRatesSetup,
        this.userPermissions});

  WebSetupAccess.fromJson(Map<String, dynamic> json) {
    dataExportConfig = json['dataExportConfig'];
    discountsSetup = json['discountsSetup'];
    financialAccounts = json['financialAccounts'];
    kitchenOrDiningRoomSetup = json['kitchenOrDiningRoomSetup'];
    manageInstructions = json['manageInstructions'];
    paymentsSetup = json['paymentsSetup'];
    publishing = json['publishing'];
    restaurantGroupsSetup = json['restaurantGroupsSetup'];
    restaurantOperationsSetup = json['restaurantOperationsSetup'];
    taxRatesSetup = json['taxRatesSetup'];
    userPermissions = json['userPermissions'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dataExportConfig'] = this.dataExportConfig;
    data['discountsSetup'] = this.discountsSetup;
    data['financialAccounts'] = this.financialAccounts;
    data['kitchenOrDiningRoomSetup'] = this.kitchenOrDiningRoomSetup;
    data['manageInstructions'] = this.manageInstructions;
    data['paymentsSetup'] = this.paymentsSetup;
    data['publishing'] = this.publishing;
    data['restaurantGroupsSetup'] = this.restaurantGroupsSetup;
    data['restaurantOperationsSetup'] = this.restaurantOperationsSetup;
    data['taxRatesSetup'] = this.taxRatesSetup;
    data['userPermissions'] = this.userPermissions;
    return data;
  }
}
