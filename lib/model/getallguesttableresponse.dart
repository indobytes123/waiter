class guesttablelistresponse {
  List<GuestTableList> ordersInfo;
  int responseStatus;
  String result;

  guesttablelistresponse(
      {this.ordersInfo, this.responseStatus, this.result});

  guesttablelistresponse.fromJson(Map<String, dynamic> json) {
    if (json['ordersInfo'] != null) {
      ordersInfo = new List<GuestTableList>();
      json['ordersInfo'].forEach((v) {
        ordersInfo.add(new GuestTableList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.ordersInfo != null) {
      data['ordersInfo'] =
          this.ordersInfo.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class GuestTableList {
  String createdBy;
  String createdOn;
  String orderId;
  int noOfGuest;
  String restaurantId;
  String serviceAreaId;
  int status;
  int tableNumber;
  int orderNumber;
  String tableId;

  GuestTableList(
      {this.createdBy,
        this.createdOn,
        this.orderId,
        this.noOfGuest,
        this.restaurantId,
        this.serviceAreaId,
        this.status,
        this.tableNumber,
      this.orderNumber,
        this.tableId
      });

  GuestTableList.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdOn = json['createdOn'];
    orderId = json['orderId'];
    noOfGuest = json['noOfGuest'];
    restaurantId = json['restaurantId'];
    serviceAreaId = json['serviceAreaId'];
    status = json['status'];
    tableNumber = json['tableNumber'];
    orderNumber = json['orderNumber'];
    tableId = json['tableId'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdOn'] = this.createdOn;
    data['orderId'] = this.orderId;
    data['noOfGuest'] = this.noOfGuest;
    data['restaurantId'] = this.restaurantId;
    data['serviceAreaId'] = this.serviceAreaId;
    data['status'] = this.status;
    data['tableNumber'] = this.tableNumber;
    data['orderNumber'] = this.orderNumber;
    data['tableId'] = this.tableId;
    return data;
  }
}