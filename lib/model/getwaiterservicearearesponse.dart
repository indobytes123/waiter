class getwaiterserviceareasresponse {
  int responseStatus;
  String result;
  List<ServiceAreasList> serviceAreasList;

  getwaiterserviceareasresponse(
      {this.responseStatus, this.result, this.serviceAreasList});

  getwaiterserviceareasresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['service_areasList'] != null) {
      serviceAreasList = new List<ServiceAreasList>();
      json['service_areasList'].forEach((v) {
        serviceAreasList.add(new ServiceAreasList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.serviceAreasList != null) {
      data['service_areasList'] =
          this.serviceAreasList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ServiceAreasList {
  String id;
  bool primary;
  String revenueCenter;
  String revenueCenterName;
  String serviceChargeId;
  String serviceName;

  ServiceAreasList(
      {this.id,
        this.primary,
        this.revenueCenter,
        this.revenueCenterName,
        this.serviceChargeId,
        this.serviceName});

  ServiceAreasList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    primary = json['primary'];
    revenueCenter = json['revenueCenter'];
    revenueCenterName = json['revenueCenterName'];
    serviceChargeId = json['serviceChargeId'];
    serviceName = json['serviceName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['primary'] = this.primary;
    data['revenueCenter'] = this.revenueCenter;
    data['revenueCenterName'] = this.revenueCenterName;
    data['serviceChargeId'] = this.serviceChargeId;
    data['serviceName'] = this.serviceName;
    return data;
  }
}


