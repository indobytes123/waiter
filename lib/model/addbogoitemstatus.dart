class addbogoitemstatus {
  String bogoid;
  List<String> menuItemStatus;

  addbogoitemstatus(this.bogoid, this.menuItemStatus);

  addbogoitemstatus.fromJson(Map<String, dynamic> json) {
    bogoid = json['bogoid'];
    menuItemStatus = json['menu_item_status'].cast<String>();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bogoid'] = this.bogoid;
    data['menu_item_status'] = this.menuItemStatus;
    return data;
  }
}