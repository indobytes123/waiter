class giftcardresponse {
  GiftCard giftCard;
  int responseStatus;
  String result;

  giftcardresponse({this.giftCard, this.responseStatus, this.result});

  giftcardresponse.fromJson(Map<String, dynamic> json) {
    giftCard = json['giftCard'] != null
        ? new GiftCard.fromJson(json['giftCard'])
        : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.giftCard != null) {
      data['giftCard'] = this.giftCard.toJson();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class GiftCard {
  String amount;
  String cardType;
  String createdOn;
  String email;
  String name;

  GiftCard({this.amount, this.cardType, this.createdOn, this.email, this.name});

  GiftCard.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    cardType = json['cardType'];
    createdOn = json['createdOn'];
    email = json['email'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['cardType'] = this.cardType;
    data['createdOn'] = this.createdOn;
    data['email'] = this.email;
    data['name'] = this.name;
    return data;
  }
}
