

import 'package:waiter/model/SendTaxRatesModel.dart';
import 'package:waiter/model/getmenuitems.dart';

class getallvoidreasonsresponse {
  List<VoidItemsList> itemsList;

  getallvoidreasonsresponse({this.itemsList});

  getallvoidreasonsresponse.fromJson(Map<String, dynamic> json) {
    if (json['itemsList'] != null) {
      itemsList = new List<VoidItemsList>();
      json['itemsList'].forEach((v) {
        itemsList.add(new VoidItemsList.fromJson(v));
      });
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.itemsList != null) {
      data['itemsList'] = this.itemsList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class VoidItemsList {
  String id;
  int voidQuantity;
  String voidId;
  String voidReason;

  VoidItemsList(this.id, this.voidQuantity, this.voidId, this.voidReason);

  VoidItemsList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    voidQuantity = json['voidQuantity'];
    voidId = json['voidId'];
    voidReason = json['voidReason'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['voidQuantity'] = this.voidQuantity;
    data['voidId'] = this.voidId;
    data['voidReason'] = this.voidReason;
    return data;
  }
}