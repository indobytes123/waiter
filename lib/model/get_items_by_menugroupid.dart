import 'package:waiter/model/SendTaxRatesModel.dart';

class get_items_by_menugroupid {
  List<MenuItems> menuItems;
  int responseStatus;
  String result;

  get_items_by_menugroupid({this.menuItems, this.responseStatus, this.result});

  get_items_by_menugroupid.fromJson(Map<String, dynamic> json) {
    if (json['menuItems'] != null) {
      menuItems = new List<MenuItems>();
      json['menuItems'].forEach((v) { menuItems.add(new MenuItems.fromJson(v)); });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menuItems != null) {
      data['menuItems'] = this.menuItems.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class MenuItems {
  String basePrice;
  bool diningOptionTaxException;
  bool diningTaxOption;
  int discountAmount;
  String discountId;
  String discountName;
  String discountType;
  int discountValue;
  bool isBogo;
  bool isCombo;
  String itemId;
  String itemName;
  String itemType;
  String menuGroupId;
  String menuId;
  int pricingStrategy;
  int quantity;
  List<SizeList> sizeList;
  bool taxIncludeOption;
  List<SendTaxRatesModel> taxesList;
  String type;


  MenuItems(this.basePrice,this.diningOptionTaxException, this.diningTaxOption, this.discountAmount, this.discountId, this.discountName, this.discountType, this.discountValue, this.isBogo, this.isCombo, this.itemId, this.itemName, this.itemType, this.menuGroupId, this.menuId, this.pricingStrategy, this.quantity, this.sizeList, this.taxIncludeOption, this.type);

  MenuItems.fromJson(Map<String, dynamic> json) {
    basePrice = json['basePrice'];
    diningOptionTaxException = json['diningOptionTaxException'];
    diningTaxOption = json['diningTaxOption'];
    discountAmount = json['discountAmount'];
    discountId = json['discountId'];
    discountName = json['discountName'];
    discountType = json['discountType'];
    discountValue = json['discountValue'];
    isBogo = json['isBogo'];
    isCombo = json['isCombo'];
    itemId = json['itemId'];
    itemName = json['itemName'];
    itemType = json['itemType'];
    menuGroupId = json['menuGroupId'];
    menuId = json['menuId'];

    pricingStrategy = json['pricingStrategy'];
    quantity = json['quantity'];
    if (json['sizeList'] != null) {
      sizeList = new List<SizeList>();
      json['sizeList'].forEach((v) { sizeList.add(new SizeList.fromJson(v)); });
    }else{
      sizeList = [];
    }

    taxIncludeOption = json['taxIncludeOption'];
    if (json['taxesList'] != null) {
      taxesList = new List<SendTaxRatesModel>();
      json['taxesList'].forEach((v) { taxesList.add(new SendTaxRatesModel.fromJson(v)); });
    }
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basePrice'] = this.basePrice;
    data['diningOptionTaxException'] = this.diningOptionTaxException;
    data['diningTaxOption'] = this.diningTaxOption;
    data['discountAmount'] = this.discountAmount;
    data['discountId'] = this.discountId;
    data['discountName'] = this.discountName;
    data['discountType'] = this.discountType;
    data['discountValue'] = this.discountValue;
    data['isBogo'] = this.isBogo;
    data['isCombo'] = this.isCombo;
    data['itemId'] = this.itemId;
    data['itemName'] = this.itemName;
    data['itemType'] = this.itemType;
    data['menuGroupId'] = this.menuGroupId;
    data['menuId'] = this.menuId;

    data['pricingStrategy'] = this.pricingStrategy;
    data['quantity'] = this.quantity;
    if (this.sizeList != null) {
      data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    }

    data['taxIncludeOption'] = this.taxIncludeOption;
    if (this.taxesList != null) {
      data['taxesList'] = this.taxesList.map((v) => v.toJson()).toList();
    }
    data['type'] = this.type;
    return data;
  }
}

class SizeList {
  double price;
  String sizeName;

  SizeList(this.price, this.sizeName);

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'];
    sizeName = json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}

/*class TaxesList {
  bool tdefault;
  bool enableTakeOutRate;
  String importId;
  int orderValue;
  int roundingOptions;
  int status;
  String taxName;
  double taxRate;
  List<TaxTable> taxTable;
  int taxType;
  String taxid;
  int uniqueNumber;

  TaxesList(this.tdefault, this.enableTakeOutRate, this.importId, this.orderValue, this.roundingOptions, this.status, this.taxName, this.taxRate, this.taxTable, this.taxType, this.taxid, this.uniqueNumber);

  TaxesList.fromJson(Map<String, dynamic> json) {
    tdefault = json['default'];
    enableTakeOutRate = json['enableTakeOutRate'];
    importId = json['importId'];
    orderValue = json['orderValue'];
    roundingOptions = json['roundingOptions'];
    status = json['status'];
    taxName = json['taxName'];
    taxRate = json['taxRate'];
    if (json['taxTable'] != null) {
      taxTable = new List<TaxTable>();
      json['taxTable'].forEach((v) { taxTable.add(new TaxTable.fromJson(v)); });
    }
    taxType = json['taxType'];
    taxid = json['taxid'];
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['default'] = this.tdefault;
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    if (this.taxTable != null) {
      data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    }
    data['taxType'] = this.taxType;
    data['taxid'] = this.taxid;
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}*/
/*
class TaxesList {
  //bool default;


  bool enableTakeOutRate;
  String importId;
  int orderValue;
  int roundingOptions;
  int status;
  String taxName;
  double taxRate;
  String taxType;
  */
/* int taxTypeId;*//*

  List<TaxTable> taxTable;
  String taxid;
  int uniqueNumber;
  double tax;

  TaxesList(
      this.enableTakeOutRate, this.importId, this.orderValue, this.roundingOptions, this.status, this.taxName, this.taxRate,this.taxType,*/
/*this.taxTypeId,*//*
 this.taxTable,  this.taxid, this.uniqueNumber,this.tax);

  TaxesList.fromJson(Map<String, dynamic> json) {




    enableTakeOutRate = json['enableTakeOutRate'];
    importId = json['importId'] as String;
    orderValue = json['orderValue'] as int;
    roundingOptions = json['roundingOptions'] as int;
    status = json['status'] as int;
    taxName = json['taxName'] as String;
    taxRate = json['taxRate'] as double;
    taxType = json['taxType'] as String;
    */
/*   taxTypeId = json['taxTypeId'] as int;*//*

    if (json['taxTable'] != null) {
      taxTable = new List<TaxTable>();
      json['taxTable'].forEach((v) {
        taxTable.add(new TaxTable.fromJson(v));
      });
    }
    taxid = json['taxid'] as String;
    uniqueNumber = json['uniqueNumber'] as int;
    tax = json['tax'] as double;
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    data['taxType'] = this.taxType;
    */
/*  data['taxTypeId'] = this.taxTypeId;*//*

    if (this.taxTable != null) {
      data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    }
    data['taxid'] = this.taxid;
    data['uniqueNumber'] = this.uniqueNumber;
    data['tax'] = this.tax;
    return data;
  }

}

class TaxTable {
  String from;
  double priceDifference;
  bool repeat;
  String taxApplied;
  String to;

  TaxTable({ this.from, this.priceDifference, this.repeat, this.taxApplied,this.to});

  TaxTable.fromJson(Map<String, dynamic> json) {
    from = json['from'];
    priceDifference = json['priceDifference'] as double;
    repeat = json['repeat'];
    taxApplied = json['taxApplied'];
    to = json['to'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['from'] = this.from;
    data['priceDifference'] = this.priceDifference;
    data['repeat'] = this.repeat;
    data['taxApplied'] = this.taxApplied;
    data['to'] = this.to;
    return data;
  }
}*/
